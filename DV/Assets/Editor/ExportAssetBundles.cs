﻿// C# Example
// Builds an asset bundle from the selected objects in the project view.
// Once compiled go to "Menu" -> "Assets" and select one of the choices
// to build the Asset Bundle

using UnityEngine;
using UnityEditor;

public class ExportAssetBundles {
	[MenuItem("Assets/Build AssetBundle From Selection - With bundle folder")]
	static void ExportResourceWithFolder () {
		string folder = "";
		BuildTarget target;
		// Bring up save panel
		#if UNITY_IPHONE	
		folder = "ios/";
		target = BuildTarget.iOS;
		#elif UNITY_ANDROID
		folder = "android/";
		target = BuildTarget.Android;
		#elif UNITY_WEBPLAYER
		folder = "web/";
		target = BuildTarget.WebPlayer;
		#elif UNITY_WEBGL
		folder = "web/";
		target = BuildTarget.WebGL;
		#endif	

		foreach(Object obj in Selection.gameObjects){
			GameObject go = (GameObject)obj;
			string path = "../../Garden Visualiser/distribute/www/asset_bundles/" + folder + go.name + ".unity3d";
			//Debug.Log (path);
			if (path.Length != 0) {
				// Build the resource file from the game object.
				GameObject[] objects = new GameObject[go.transform.childCount];
				for(int i=0; i<go.transform.childCount; i++){
					Transform t = go.transform.GetChild(i);
					if (t!=null) objects[i] = t.gameObject;
				}
				BuildPipeline.BuildAssetBundle(go, objects, path, BuildAssetBundleOptions.CollectDependencies | BuildAssetBundleOptions.CompleteAssets, target);
			}
		}
	}
}