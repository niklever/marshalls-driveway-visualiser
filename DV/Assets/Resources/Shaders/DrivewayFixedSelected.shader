﻿Shader "Custom/DrivewayFixedSelected" {
	Properties { 
		_Color ( "Main Color", Color) = (1,1,1,1)
		_Emission ("Emissive Color", Color) = (0.3,0.3,0.3,1)
		_MainTex ("Base", 2D) = "white" {}
	}
	SubShader {
		Tags { "RenderType"="Opaque" }
		Pass {
			Material { Diffuse (1,1,1,0) Ambient (1,1,1,0) Emission [_Emission] }
			Lighting On
			SetTexture [_MainTex] {
				matrix [_Rotation]
	           constantColor[_Color]
	           combine primary * texture
			}
			SetTexture [_MainTex] {
				matrix [_Rotation]
	          	constantColor[_Emission]
	           	combine previous + constant
			}
		}
	}
	FallBack "Diffuse"
}
