﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Ucss;
using SimpleJSON;
using JsonFx.Json;
using Assets.Scripts.models;

public class DVWebService{
	private static string _dvEndPoint = "https://www.marshalls.co.uk/test-visualizer/services-dv/api/";
	public static string EndPoint{
		get{
			return _dvEndPoint;
		}
	}
	public static string ResourcePath{
		get{
			return "https://www.marshalls.co.uk/test-visualizer/dev/";
		}
	}
	private static string _token;
	public static string Token{
		get{
			return _token;
		}
	}
	private static string _email;
	private static string _password;
	private static string _tempEmail;
	private static string _tempPassword;
	private static DV_UI _dv_ui;
	static bool _loadProjectsOnComplete;
	static bool _saveProject;
	static Project _project;
	private static List<string>_cloneGuids;
	public static List<string>CloneGuids{
		set{
			_cloneGuids = value;
			if (_cloneGuids.Count>0){
				CloneProject(_cloneGuids[0]);
			}
		}
	}
	static float _plannerVersion = 2.0f;
	static InstallerStockistController _isController;
	static bool _showProjectsPanel = true;//Default

	static DV_UI DV_UI{
		get{
			if (_dv_ui==null){
				GameObject go = GameObject.Find ("Canvas");
				_dv_ui = go.GetComponent<DV_UI>();
			}
			return _dv_ui;
		}
	}
	private static ProjectsController _projectsController;
	static ProjectsController ProjectsController{
		get{
			if (_projectsController==null){
				GameObject go = GameObject.Find ("Canvas/ProjectsPanel");
				_projectsController = go.GetComponent<ProjectsController>();
			}
			return _projectsController;
		}
	}
	private static SettingsController _settingsController;
	static SettingsController SettingsController{
		get{
			if (_settingsController==null){
				GameObject go = GameObject.Find ("Canvas/ModalPanels/SettingsPanel");
				_settingsController = go.GetComponent<SettingsController>();
			}
			return _settingsController;
		}
	}
	static DrivewayController _drivewayController;
	public static DrivewayController DrivewayController{
		get{
			if (_drivewayController==null){
				GameObject go = GameObject.Find ("3D");
				_drivewayController = go.GetComponent<DrivewayController>();
			}
			return _drivewayController;
		}
		set{
			_drivewayController = value;
		}
	}
	ThumbnailController _thumbnailController = null;
	static ForgotController _forgotController = null;
	static int _loginCount = 0;

	public static void LoadProductsConfig()
	{
		string url = ResourcePath + "data/products.json";
		Debug.Log("DVWebService.LoadProductsConfig: url:" + url);
		
		UCSS.HTTP.GetString ( url, DVWebService.LoadProductsConfigCallback, DVWebService.LoadProductsConfigError);
	}

	static void LoadProductsConfigError(string error, string id){
		Debug.Log("DVWebService.LoadProductsConfig download error:" + error);
	}

	static void LoadProductsConfigCallback(string str, string id){
		Debug.Log("DVWebService.LoadProductsConfig products:" + str);
		DrivewayController.ProductsJSON = JSON.Parse(str);
		LoadPatterns();
	}

	public static void LoadPatterns()
	{
		string url = ResourcePath + "data/patterns.json";
		Debug.Log("DVWebService.LoadPatterns: url:" + url);
		
		UCSS.HTTP.GetString ( url, DVWebService.LoadPatternsCallback, DVWebService.LoadPatternsError);
	}
	
	static void LoadPatternsError(string error, string id){
		Debug.Log("DVWebService.LoadPatterns download error:" + error);
	}
	
	static void LoadPatternsCallback(string str, string id){
		Debug.Log("DVWebService.LoadPatterns products:" + str);
		Driveway.PatternsJSON = JSON.Parse(str);
		Driveway.ResourcePath = DVWebService.ResourcePath;
		LoadFeatures();
	}

	public static void LoadFeatures()
	{
		string url = ResourcePath + "data/features.json";
		Debug.Log("DVWebService.LoadFeatures: url:" + url);
		
		UCSS.HTTP.GetString ( url, DVWebService.LoadFeaturesCallback, DVWebService.LoadFeaturesError);
	}
	
	static void LoadFeaturesError(string error, string id){
		Debug.Log("DVWebService.LoadFeatures download error:" + error);
	}
	
	static void LoadFeaturesCallback(string str, string id){
		Debug.Log("DVWebService.LoadFeatures products:" + str);
		Feature.FeaturesJSON = JSON.Parse(str);
		Feature.ResourcePath = DVWebService.ResourcePath;
	}

	public static void LoginHandler(string username, string password)
	{	
		string url = _dvEndPoint + "Login?username=" + username + "&password=" + password;
		Debug.Log("DVWebService LoginHandler url:" + url + " username:" + username + " pword:" + password);
		_email = username;
		_password = password;
		if (DV_UI) _dv_ui.Busy = true;

		UCSS.HTTP.GetString ( url, DVWebService.LoginCallback, DVWebService.LoginError);
	}

	static void LoginError(string error, string id){
		Debug.Log("DVWebService.Login download error:" + error);
		if (error.StartsWith ("409")) {
			ForgottenPassword (null);
		} else if (error.StartsWith ("404")) {
			DV_UI.ShowError (error.Substring (4));
			if (DV_UI)
				_dv_ui.Busy = false;
			_loginCount = 0;
		}else if (_loginCount < 3) {
			_loginCount++;
			LoginHandler (_email, _password);
		} else {
			DV_UI.ShowError (string.Format ("There was a problem {0}", error));
			if (DV_UI)
				_dv_ui.Busy = false;
			_loginCount = 0;
		}
	}
	
	static void LoginCallback(string str, string id){
		Debug.Log("DVWebService.Login returned " + str);
		_loginCount = 0;
		JSONNode json = JSON.Parse(str);
		int responseCode = json["ResponseStatusCode"].AsInt;
		switch(responseCode){
		case 200:
			_token = json["Token"];
			PlayerPrefs.SetString("email", _email);
			PlayerPrefs.SetString("password", _password);
			LoadProjects();
			break;
		case 401:
			DV_UI.ShowAlert("Login", "Sorry this password is incorrect.");
			if (DV_UI) _dv_ui.Busy = false;
			break;
		case 404:
			DV_UI.ShowAlert("Login", "Sorry this account does not exist.\nPlease create a new account.");
			if (DV_UI) _dv_ui.Busy = false;
			break;
		case 409:
			ForgottenPassword (null);
			break;
		default:
			DV_UI.ShowAlert("Login", "Sorry there is an error with the email or the password.");
			if (DV_UI) _dv_ui.Busy = false;
			break;
		}
	}

	public static void LoadProjects(bool show=true){
		string url = string.Format("{0}Project?authentication-token={1}&thumbWidth={2}&thumbHeight={3}&thumbResizeMode=4", _dvEndPoint, _token, 416, 336);
		Debug.Log("DVWebService.LoadProject url:" + url);
		
		UCSS.HTTP.GetString ( url, DVWebService.LoadProjectsCallback, DVWebService.LoadProjectsError);
		_showProjectsPanel = show;
	}
		
	static void LoadProjectsError(string error, string id){
		Debug.Log("DVWebService.LoadProjects download error:" + error);
		if (DV_UI) _dv_ui.Busy = false;
	}
	
	static void LoadProjectsCallback(string str, string id){
		Debug.Log("DVWebService.LoadProjects returned " + str);
		JSONNode json = JSON.Parse(str);
		int responseCode = json["ResponseStatusCode"].AsInt;
		if (DV_UI) _dv_ui.Busy = false;
		switch(responseCode){
		case 0:
			if (ProjectsController) _projectsController.UpdatePanel(json, _showProjectsPanel);
			_showProjectsPanel = true;//Default
			break;
		default:
			DV_UI.ShowAlert("Projects", json["Message"]);
			break;
		}
	}

	public void GetThumbnail(string url, ThumbnailController controller){
		Debug.Log ("DVWebService.GetThumbnail url:" + url);
		_thumbnailController = controller;
		
		UCSS.HTTP.GetBytes (url, new EventHandlerHTTPBytes(this.GetThumbnailCallback), new EventHandlerServiceError(this.GetThumbnailError));
	}
	
	void GetThumbnailError(string error, string id){
		_thumbnailController.WSError("DVWebService.GetThumbnail download error:" + error);
	}
	
	void GetThumbnailCallback(byte[] bytes, string id){
		Texture2D tex = new Texture2D(2, 2);
		if (!tex.LoadImage(bytes)) {
			_thumbnailController.WSError("DVWebService.GetThumbnail Failed loading image data!");
		}else {
			tex.Apply ();
			//Debug.Log(String.Format ("DVWebService.GetThumbnail LoadImage - size:{0}x{1} bytes length:{2}", tex.width, tex.height, bytes.Length ));
		}
		_thumbnailController.Thumbnail = tex;
	}

	public static void CreateAccount(string firstname, string lastname, string username, string password)
	{
		if (firstname.Equals(""))
		{
			DV_UI.ShowAlert("ERROR", "Please enter your firstname");
			return;
		}
		else if (lastname.Equals(""))
		{
			DV_UI.ShowAlert("ERROR", "Please enter your lastname");
			return;
		}
		else if (username.Equals(""))
		{
			DV_UI.ShowAlert("ERROR", "Please enter your email address");
			return;
		}
		else if (password.Equals(""))
		{
			DV_UI.ShowAlert("ERROR", "Please enter a password");
			return;
		}
		
		string url = _dvEndPoint + "Account";
		
		Debug.Log("LoginScript.CreateAccount");
		
		Dictionary<string,string> headers = new Dictionary<string,string>();
		headers.Add("Content-Type", "application/json");
		headers.Add("Accept", "application/json");
		headers.Add("X-Test-Custom-Header", "Hello Rich!");
		
		JsonDataWriter writer = new JsonDataWriter(new JsonWriterSettings());
		
		CreateAccountModel accountDetails = new CreateAccountModel
		{
			UserName = username,
			Password = password,
			FirstName = firstname,
			LastName = lastname
		};

		_tempEmail = username;
		_tempPassword = password;

		byte[] postData = writer.SerializeToByteArray(accountDetails);
		
		UCSS.HTTP.PostBytes(url, postData, headers, new EventHandlerHTTPString(DVWebService.CreateAccountCallback), new EventHandlerServiceError(DVWebService.CreateAccountError));
	}

	static void CreateAccountError(string error, string id){
		Debug.Log("DVWebService.CreateAccount download error:" + error);
		_email = _password = "";
	}
	
	static void CreateAccountCallback(string data, string id){
		Debug.Log("DVWebService.CreateAccount data:" + data);
		JSONNode json = JSON.Parse(data);
		int responseCode = json["ResponseStatusCode"].AsInt;
		string message = json ["ResponseStatusMessage"].ToString ();
		switch(responseCode){
		case 200:
			_token = json["Token"];
			_email = _tempEmail;
			_password = _tempPassword;
			PlayerPrefs.SetString("email", _email);
			PlayerPrefs.SetString("password", _password);
			LoadProjects();
			break;
		case 409:
			DV_UI.ShowAlert("Create Account", message);
			break;
		default:
			DV_UI.ShowAlert("Create Account", message);
			break;
		}
		if (DV_UI) _dv_ui.Busy = false;
	}
	
	public static bool UpdateAccount(string firstname, string lastname, string username, string password)
	{	
		if (firstname.Equals(""))
		{
			DV_UI.ShowAlert("ERROR", "Please enter your firstname");
			return false;
		}
		else if (lastname.Equals(""))
		{
			DV_UI.ShowAlert("ERROR", "Please enter your lastname");
			return false;
		}
		else if (username.Equals(""))
		{
			DV_UI.ShowAlert("ERROR", "Please enter your email address");
			return false;
		}
		else if (password.Equals(""))
		{
			DV_UI.ShowAlert("ERROR", "Please enter a password");
			return false;
		}

		string url;
		
		url = _dvEndPoint + "Account";
		
		Debug.Log("LoginScript.UpdateAccount");
		
		Dictionary<string,string> headers = new Dictionary<string,string>();
		headers.Add("Content-Type", "application/json");
		headers.Add("Accept", "application/json");
		headers.Add ("Authentication-Token", _token);
		headers.Add ("X-HTTP-Method-Override", "PUT");
		
		JsonDataWriter writer = new JsonDataWriter(new JsonWriterSettings());
		
		CreateAccountModel accountDetails = new CreateAccountModel
		{
			UserName = username,
			Password = password,
			FirstName = firstname,
			LastName = lastname
		};

		_tempEmail = username;
		_tempPassword = password;
		
		byte[] postData = writer.SerializeToByteArray(accountDetails);
		
		UCSS.HTTP.PostBytes(url, postData, headers, new EventHandlerHTTPString(DVWebService.UpdateAccountCallback), new EventHandlerServiceError(DVWebService.UpdateAccountError));
		if (DV_UI) _dv_ui.Busy = true;

		return true; 
	}

	static void UpdateAccountError(string error, string id){
		Debug.Log("DVWebService.UpdateAccount download error:" + error);
		DV_UI.ShowAlert ("ERROR", "There was a problem updating your account details");
		if (DV_UI) _dv_ui.Busy = false;
	}
	
	static void UpdateAccountCallback(string data, string id){
		Debug.Log("DVWebService.UpdateAccount data:" + data);
		//Save revised info
		_email = _tempEmail;
		_password = _tempPassword;
		PlayerPrefs.SetString("email", _email);
		PlayerPrefs.SetString("password", _password);
		if (ProjectsController) _projectsController.SettingsClosePressed();
		if (DV_UI) _dv_ui.Busy = false;
		DV_UI.ShowAlert ("Update", "Your account details have been updated");
	}

	public static void GetAccount()
	{
		if (_token==null || _token.Equals(""))
		{
			DV_UI.ShowAlert("ERROR", "You need to be logged in to use this function");
			return;
		}
		
		string url = _dvEndPoint + string.Format("Account?authentication-token={0}", _token);
		
		Debug.Log("DVWebService.GetAccount " + url);

		if (DV_UI) _dv_ui.Busy = true;
		
		UCSS.HTTP.GetString ( url, DVWebService.GetAccountCallback, DVWebService.GetAccountError);
	}

	static void GetAccountError(string error, string id){
		Debug.Log("DVWebService.GetAccount download error:" + error);
		DV_UI.ShowAlert("ERROR", "There was a problem accessing your account details. Please try later.");
		if (DV_UI) _dv_ui.Busy = false;
	}
	
	static void GetAccountCallback(string data, string id){
		Debug.Log("DVWebService.GetAccount data:" + data);
		JSONNode json = JSON.Parse(data);
		if (SettingsController!=null) _settingsController.UpdatePanel(_email, _password, json["FirstName"], json["LastName"]);
		if (DV_UI) _dv_ui.Busy = false;
	}
	
	public static void SaveNewProject(Project project)
	{
		string xml = project.ToXML();
		Debug.Log("DVWebService.SaveNewProject xml:" + xml);
		
		xml = xml.Replace("\n", "");
		xml = xml.Replace("\t", "");
		xml = xml.Replace("\"", "\\\"");
		
		string url;
		
		JsonDataWriter writer = new JsonDataWriter(new JsonWriterSettings());
		
		CreateNewProjectModel projectDetails = new CreateNewProjectModel
		{
			PlannerVersion = _plannerVersion,
			Name = project.Name,
			Size = project.Size,
			Xml = project.ToXML(),
			ProjectImageFileContent = project.Thumbnail
		};
		
		byte[] postData = writer.SerializeToByteArray(projectDetails);

		url = _dvEndPoint + "Project";
		
		Debug.Log("DVWebService.SaveNewProject data:" + writer.SerializeToString(projectDetails));
		Dictionary<string,string> headers = new Dictionary<string,string>();
		headers.Add("Content-Type", "application/json");
		headers.Add("Accept", "application/json");
		headers.Add ("Authentication-Token", _token);
		headers.Add ("X-HTTP-Method-Override", "POST");
		
		UCSS.HTTP.PostBytes(url, postData, headers, new EventHandlerHTTPString(DVWebService.SaveNewProjectCallback), new EventHandlerServiceError(DVWebService.SaveNewProjectError));
		_project = project;

		if (DV_UI) _dv_ui.Busy = true;
	}

	static void SaveNewProjectError(string error, string id){
		Debug.Log("DVWebService.SaveNewProject download error:" + error);
		DV_UI.ShowError (error);
		DV_UI.CloseModalPanel ();
		DV_UI.Animate("designOut");
		if (DV_UI) _dv_ui.Busy = false;
	}
	
	static void SaveNewProjectCallback(string data, string id){
		Debug.Log("DVWebService.SaveNewProject data:" + data);
		JSONNode json = JSON.Parse(data);
		int status = (json!=null && json["ResponseStatusCode"]!="null") ? json ["ResponseStatusCode"].AsInt : 0;
		if (status != 0) {
			SaveNewProjectError (json ["ResponseStatusMessage"], id);
		} else {
			_project.Guid = data;
			if (DV_UI)
				_dv_ui.Busy = false;
			LoadProjects (false);
		}
	}

	public static void SaveProject(Project project, bool loadprojects=false)
	{
		//string guid = (project.Guid.Equals("")) ? "" : ("\"Guid\": \"" + project.Guid + "\",");
		string xml = project.ToXML();
		Debug.Log("DVWebService.SaveProject xml:" + xml);
		
		xml = xml.Replace("\n", "");
		xml = xml.Replace("\t", "");
		xml = xml.Replace("\"", "\\\"");
		
		string url;
		//project.Photos.Add ("04008dfe-c787-491c-8de5-cade0a4ff5d5");
		
		JsonDataWriter writer = new JsonDataWriter(new JsonWriterSettings());
		List<ProjectPhoto> photos = new List<ProjectPhoto>();
		foreach(string photo in project.Photos) photos.Add (new ProjectPhoto(photo));
		
		CreateProjectModel projectDetails = new CreateProjectModel
		{
			Guid = project.Guid,
			PlannerVersion = _plannerVersion,
			Name = project.Name,
			Size = project.Size,
			Xml = project.ToXML(),
			ProjectImageFileContent = project.Thumbnail,
			ProjectPhotos = photos
		};
		
		Debug.Log ("DVWebService.SaveProject photos:" + writer.SerializeToString(photos));
		byte[] postData = writer.SerializeToByteArray(projectDetails);
		
		url = _dvEndPoint + "Project";
		
		Debug.Log("DVWebService.SaveProject data:" + postData);
		Dictionary<string,string> headers = new Dictionary<string,string>();
		headers.Add("Content-Type", "application/json");
		headers.Add("Accept", "application/json");
		headers.Add ("Authentication-Token", _token);
		headers.Add ("X-HTTP-Method-Override", "PUT");
		
		UCSS.HTTP.PostBytes(url, postData, headers, new EventHandlerHTTPString(DVWebService.SaveProjectCallback), new EventHandlerServiceError(DVWebService.SaveProjectError));
		_project = project;
		
		//if (DV_UI) _dv_ui.Busy = true;

		_loadProjectsOnComplete = loadprojects;
	}

	static void SaveProjectError(string error, string id){
		Debug.Log("DVWebService.SaveProject download error:" + error);
		if (DV_UI) _dv_ui.Busy = false;
	}
	
	static void SaveProjectCallback(string data, string id){
		Debug.Log ("DVWebService.SaveProject data:" + data);
		if (DV_UI)
			_dv_ui.Busy = false;
		if (_loadProjectsOnComplete){
			LoadProjects (false);
			_loadProjectsOnComplete = false;
		}
	}

	public static void DeleteProjects(List<string> guids)
	{
		string url;

		JsonDataWriter writer = new JsonDataWriter(new JsonWriterSettings());
		byte[] postData = writer.SerializeToByteArray(guids);
		
		url = _dvEndPoint + "Project";
		
		Debug.Log("LoginScript.DeleteProjects data:" + writer.SerializeToString(guids));
		Dictionary<string,string> headers = new Dictionary<string,string>();
		headers.Add("Content-Type", "application/json");
		headers.Add("Accept", "application/json");
		headers.Add("Authentication-Token", _token);
		headers.Add ("X-HTTP-Method-Override", "DELETE");
		
		UCSS.HTTP.PostBytes(url, postData, headers, new EventHandlerHTTPString(DVWebService.DeleteProjectsCallback), new EventHandlerServiceError(DVWebService.DeleteProjectsError));
		if (DV_UI) _dv_ui.Busy = true;
	}

	static void DeleteProjectsError(string error, string id){
		Debug.Log("DVWebService.DeleteProjects download error:" + error);
		if (DV_UI) _dv_ui.Busy = false;
	}
	
	static void DeleteProjectsCallback(string data, string id){
		Debug.Log("DVWebService.DeleteProjects data:" + data);
		if (DV_UI) _dv_ui.Busy = false;
	}
	
	static byte[] GetBytes(string str)
	{
		byte[] bytes = new byte[str.Length * sizeof(char)];
		System.Buffer.BlockCopy(str.ToCharArray(), 0, bytes, 0, bytes.Length);
		return bytes;
	}

	public static void GetInstallers(string postcode, bool installers, InstallerStockistController controller)
	{
		if (_token==null || _token.Equals(""))
		{
			DV_UI.ShowAlert("ERROR", "You need to be logged in to use this function");
			return;
		}
		
		postcode = postcode.Replace (" ", "%20");
		string service = (installers) ? "Installer" : "Stockist";
		string url = _dvEndPoint + string.Format("Find{0}?postcode={1}&authentication-token={2}", service, postcode, _token);
		
		Debug.Log("DVWebService.GetInstallers " + url);
		UCSS.HTTP.GetString ( url, DVWebService.GetInstallersCallback, DVWebService.GetInstallersError);
		_isController = controller;
		if (DV_UI) _dv_ui.Busy = true;
	}

	static void GetInstallersError(string error, string id){
		Debug.Log("DVWebService.GetInstallers download error:" + error);
		DV_UI.ShowError ("Problem getting the information");
		if (DV_UI) _dv_ui.Busy = false;
	}
	
	static void GetInstallersCallback(string data, string id){
		Debug.Log("DVWebService.GetInstallers data:" + data);
		JSONNode json = JSON.Parse(data);
		if (_isController!=null){
			_isController.UpdateList(json.AsArray);
			_isController = null;
		}
		if (DV_UI) _dv_ui.Busy = false;
	}

	public static void CloneProject(string guid){
		//Load the project first
		string url;

		_cloneGuids.Remove (guid);
		
		url = _dvEndPoint + string.Format("Project?authentication-token={0}&projectId={1}", _token, guid);
		Debug.Log("LoginScript.CloneProject url:" + url + " token:" + _token);
		
		UCSS.HTTP.GetString ( url, DVWebService.CloneProjectCallback, DVWebService.CloneProjectError);
	}

	static void CloneProjectError(string error, string id){
		Debug.Log("DVWebService.CloneProject download error:" + error);
		DV_UI.ShowAlert("ERROR", error);
	}
	
	static void CloneProjectCallback(string data, string id){
		Debug.Log("DVWebService.CloneProject data:" + data);
		JSONNode json = JSON.Parse(data);
		_project = new Project(json["Xml"]);
		_project.Guid = "";
		_project.Name = json["Name"] + " - Copy";
		_project.Size = json["Size"].AsFloat;
		_project.Thumbnail = _projectsController.GetBase64FromGuid(json["Guid"]);
		
		//Save the project to get a new Guid
		SaveCloneProject(_project);
		if (DV_UI) _dv_ui.Busy = false;
	}
	
	public static void SaveCloneProject(Project project)
	{
		string xml = project.ToXML();
		Debug.Log("LoginScript.SaveCloneProject xml:" + xml);
		
		xml = xml.Replace("\n", "");
		xml = xml.Replace("\t", "");
		xml = xml.Replace("\"", "\\\"");
		
		string url;
		
		JsonDataWriter writer = new JsonDataWriter(new JsonWriterSettings());
		
		CreateNewProjectModel projectDetails = new CreateNewProjectModel
		{
			PlannerVersion = _plannerVersion,
			Name = project.Name,
			Size = project.Size,
			Xml = project.ToXML(),
			ProjectImageFileContent = project.Thumbnail
		};
		
		byte[] postData = writer.SerializeToByteArray(projectDetails);

		url = _dvEndPoint + "Project";
		
		Debug.Log("LoginScript.SaveCloneProject data:" + writer.SerializeToString(projectDetails));
		Dictionary<string,string> headers = new Dictionary<string,string>();
		headers.Add("Content-Type", "application/json");
		headers.Add("Accept", "application/json");
		headers.Add ("Authentication-Token", _token);
		headers.Add ("X-HTTP-Method-Override", "POST");
		
		UCSS.HTTP.PostBytes(url, postData, headers, new EventHandlerHTTPString(DVWebService.SaveCloneCallback), new EventHandlerServiceError(DVWebService.SaveCloneError));
		if (DV_UI) _dv_ui.Busy = true;
	}

	static void SaveCloneError(string error, string id){
		Debug.Log("DVWebService.SaveClone download error:" + error);
		DV_UI.ShowAlert("ERROR", error);
	}
	
	static void SaveCloneCallback(string data, string id){
		Debug.Log("DVWebService.SaveClone data:" + data);
		_project.Guid = data;
		if (_cloneGuids.Count>0){
			CloneProject(_cloneGuids[0]);
		}else{
			LoadProjects(false);
		}

		if (DV_UI) _dv_ui.Busy = false;
	}
	
	public static void GetProject(string guid)
	{
		string url;

		Debug.Log (string.Format("GetProject guid:{0}", guid));

		url = _dvEndPoint + string.Format("Project?authentication-token={0}&projectId={1}", _token, guid);
		Debug.Log("LoginScript GetProject url:" + url + " token:" + _token);
		
		UCSS.HTTP.GetString ( url, DVWebService.GetProjectCallback, DVWebService.GetProjectError);

		if (DV_UI) _dv_ui.Busy = true;
	}

	static void GetProjectError(string error, string id){
		Debug.Log("DVWebService.GetProject download error:" + error);
		DV_UI.ShowAlert("ERROR", error);
		if (DV_UI) _dv_ui.Busy = false;
	}
	
	static void GetProjectCallback(string data, string id){
		Debug.Log("DVWebService.GetProject data:" + data);
		JSONNode json = JSON.Parse(data);
		_project = new Project(json["Xml"]);
		_project.Guid = json["Guid"];
		_project.Name = json["Name"];
		_project.Size = json["Size"].AsFloat;
		JSONArray photos = (JSONArray)json["ProjectPhotos"];
		_project.Photos = new List<string>();
		for(int i=0; i<photos.Count; i++){
			JSONNode photo = photos[i];
			_project.Photos.Add (photo["Guid"]);
		}

		_drivewayController.Init(_project);
		
		if (DV_UI){
			_dv_ui.Busy = false;
			DV_UI.ShowDesignPanel(true);
			DV_UI.Animate ("designIn");
		}
	}

	public static void SendPDF(string email){
		if (_token == null || _token.Equals ("")) {
			DV_UI.ShowAlert ("ERROR", "You need to be logged in to use this function");
			return;
		}

		Debug.Log ("DVWebService.SendEmail");

		JsonDataWriter writer = new JsonDataWriter (new JsonWriterSettings ());

		string[] shots = null;

		ProjectPDF projectPDF = new ProjectPDF {
			ProjectId = DrivewayController.Project.Guid,
			EmailAddress = email
		};

		byte[] postData = writer.SerializeToByteArray (projectPDF);

		_dv_ui.Busy = true;

		string url = _dvEndPoint + "PDF";

		Debug.Log ("DVWebService.GeneratePDF data:" + postData);
		Dictionary<string,string> headers = new Dictionary<string,string>();
		headers.Add ("Content-Type", "application/json");
		headers.Add ("Accept", "application/json");
		headers.Add ("Authentication-Token", _token);
		headers.Add ("X-HTTP-Method-Override", "POST");

		UCSS.HTTP.PostBytes (url, postData, headers, new EventHandlerHTTPString (DVWebService.SendPDFCallback), new EventHandlerServiceError (DVWebService.SendPDFError));

	}

	static void SendPDFError(string data, string id){
		_dv_ui.Busy = false;
		DV_UI.ShowAlert ("Warning", "There was a problem please try later");
		Debug.Log("GVWebService.SendPDF download error:" + data);
	}

	static void SendPDFCallback(string data, string id){
		_dv_ui.Busy = false;
		DV_UI.ShowAlert ("Info", "Look out for the PDF in your email");
	}

	public static void UploadImage(byte[] bytes, bool saveProject=false){
		// Create a Web Form
		WWWForm form = new WWWForm();
		form.AddBinaryData("FileContent", bytes, "projectimage.png", "image/png");
		
		// Upload to a cgi script
		string url = string.Format ("{0}ProjectPhoto?authentication-token={1}", _dvEndPoint, _token);
		Debug.Log ("DVWebService.UploadImage " + url);
		UCSS.HTTP.PostForm ( url, form, DVWebService.UploadImageCallback, DVWebService.UploadImageError);

		_saveProject = saveProject;
		if (DV_UI) _dv_ui.Busy = true;
	}

	static void UploadImageError(string error, string id){
		Debug.Log("DVWebService.UploadImage download error:" + error);
		DV_UI.ShowAlert("ERROR", error);
		if (DV_UI) _dv_ui.Busy = false;
		_saveProject = false;
		DV_UI.CloseModalPanel ();
	}
	
	static void UploadImageCallback(string data, string id){
		Debug.Log("DVWebService.UploadImage data:" + data);
		JSONNode json = JSON.Parse(data);
		int status = (json!=null && json["ResponseStatusCode"]!="null") ? json ["ResponseStatusCode"].AsInt : 0;
		if (status==500) {
			UploadImageError (json ["ResponseStatusMessage"], id);
		}else if (json["Url"]!=null){
			if (_saveProject){
				_project = DrivewayController.Project;
				_project.HousePattern = json["Url"];
				_project.Photos.Add (json["Guid"]);
				SaveNewProject(_project);
				_saveProject = false;
				if (DrivewayController) _drivewayController.Init(_project, true);
				DV_UI.ShowDesignPanel (true);
				GameObject go = GameObject.Find ("Canvas/ModalPanels/PhotoPanel");
				PhotoController pc = go.GetComponent<PhotoController>();
				pc.Show (true);
			}
		}
	}

	public static void LoadProjectImage(string url){
		Debug.Log ("DVWebService.LoadProjectImage url:" + url);

		UCSS.HTTP.GetBytes (url, new EventHandlerHTTPBytes(DVWebService.LoadProjectImageCallback), new EventHandlerServiceError(DVWebService.LoadProjectImageError));
	}
	
	static void LoadProjectImageError(string error, string id){
		DV_UI.ShowAlert("ERROR", error);
	}
	
	static void LoadProjectImageCallback(byte[] bytes, string id){
		Texture2D tex = new Texture2D(2, 2);
		if (!tex.LoadImage(bytes)) {
			DV_UI.ShowAlert("ERROR", "DVWebService.GetThumbnail Failed loading image data!");
		}else {
			tex.Apply ();
			//Debug.Log(String.Format ("DVWebService.GetThumbnail LoadImage - size:{0}x{1} bytes length:{2}", tex.width, tex.height, bytes.Length ));
		}
		DV_UI.NewProjectImage(tex);
	} 

	public static void ForgottenPassword(ForgotController forgotController)
	{
		_forgotController = forgotController;
		string url;
		if (forgotController == null) {
			url = string.Format ("{0}ForgottenPassword?username={1}", _dvEndPoint, _email);
		} else {
			url = string.Format ("{0}ForgottenPassword?username={1}", _dvEndPoint, _forgotController.UserName);
		}
		Debug.Log("GVWebService.ForgottenPassword: url:" + url);

		UCSS.HTTP.GetString ( url, DVWebService.ForgottenPasswordCallback, DVWebService.ForgottenPasswordError);
	}

	static void ForgottenPasswordError(string error, string id){
		Debug.Log("DVWebService.ForgottenPassword download error:" + error);
		DV_UI.ShowAlert ("Warning", "There was a problem, please try later");
		_forgotController = null;
	}

	static void ForgottenPasswordCallback(string str, string id){
		Debug.Log("DVWebService.ForgottenPassword products:" + str);
		if (_forgotController != null) { 
			_forgotController.ForgottenPasswordSuccess ();
		} else {
			DV_UI.ShowAlert ("Information", string.Format("We have sent an email to {0} with a link that will allow you to create a new password for this account.", _email));
			DV_UI.Busy = false;
		}
		_forgotController = null;
	}
}
