﻿using UnityEngine;
using Vectrosity;
using System.Collections;
using System.Collections.Generic;

public class TestDrawing : MonoBehaviour {
	public Texture2D _dotTexture;

	VectorLine _line;
	VectorLine _dots;
	GameObject _lineGO;
	bool _drawing = false;
	bool _closed = false;
	int _dragIndex = -1;
	float _clickTime;

	// Use this for initialization
	void Start () {
		_lineGO = GameObject.Find ("Canvas/Line");
		VectorObject2D line = _lineGO.GetComponent<VectorObject2D>();
		_line = line.vectorLine;
		_lineGO.SetActive(false);
		_line.points2.Clear();
		_clickTime = Time.time;
		_dots = new VectorLine("Dots", new List<Vector2>(), _dotTexture, 10.0f, LineType.Points);
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetMouseButtonDown(0)){
			Vector2 mousePosition = new Vector2(Input.mousePosition.x, Input.mousePosition.y);
			float elapsedTime = Time.time - _clickTime;
			bool doubleClick = (elapsedTime<0.5f);
			_dragIndex = -1;
			int insertIndex = -1;
			int index;

			if (!_drawing){
				if (_line.points2.Count>0){
					for(int i=0; i<_line.points2.Count; i++){
						if (Vector2.Distance (_line.points2[i], mousePosition)<15){
							_dragIndex = i;
							break;
						}
					}
					if (_dragIndex==-1){
						_line.Selected (mousePosition, 15, out index);
						insertIndex = index;
					}

					if (doubleClick && _dragIndex!=-1 && insertIndex==-1){
						_line.points2.RemoveAt(_dragIndex);
						_dots.points2.RemoveAt (_dragIndex);
						_line.Draw ();
						_dots.Draw ();
						_dragIndex = -1;
					}
				}

				if (_dragIndex==-1 && insertIndex==-1 && !doubleClick){
					_line.points2.Clear();
					_dots.points2.Clear();
					_drawing = true;
					_closed = false;
					_line.points2.Add (mousePosition);
					_dots.points2.Add (mousePosition);
				}
				_lineGO.SetActive (true);
			}

			if (doubleClick && _line.points2.Count>0 && _closed){
				if (_dragIndex==-1){
					_line.Selected (mousePosition, 15, out index);
					if (index!=-1){
						if (index<_line.points2.Count){
							_line.points2.Insert(index+1, mousePosition);
						}else{
							_line.points2.Add (mousePosition);
						}
						if (index<_dots.points2.Count){
							_dots.points2.Insert(index+1, mousePosition);
						}else{
							_dots.points2.Add (mousePosition);
						}
						_line.Draw ();
						_dots.Draw ();
						insertIndex = index;
					}
				}
			}

			//Debug.Log ("TestDrawing.OnMouseDown " + Input.mousePosition);
			if (_dragIndex==-1 && insertIndex==-1){
				if (doubleClick){
					//Close shape
					_drawing = false;
					_closed = true;
					if (_line.points2.Count>0){
						_line.points2[_line.points2.Count-1] = _line.points2[0];
						_line.Draw ();
						if (_dots.points2.Count>0) _dots.points2.RemoveAt(_dots.points2.Count-1);
					}
				}else{
					//Add new point
					_line.points2.Add (mousePosition);
					_dots.points2.Add (mousePosition);
				}
			}
			_clickTime = Time.time;
		}

		if (!Input.GetMouseButton(0)) _dragIndex = -1;

		if (_drawing && _line.points2.Count>0){
			Vector2 point = _line.points2[_line.points2.Count-1];
			point.x = Input.mousePosition.x;
			point.y = Input.mousePosition.y;
			_line.points2[_line.points2.Count-1] = point;
			_line.Draw ();
			_dots.points2[_dots.points2.Count-1] = point;
			_dots.Draw ();
		}else if (_dragIndex!=-1){
			Vector2 point = _line.points2[_dragIndex];
			point.x = Input.mousePosition.x;
			point.y = Input.mousePosition.y;
			_line.points2[_dragIndex] = point;
			if (_dragIndex==0) _line.points2[_line.points2.Count-1] = point;
			_line.Draw ();
			_dots.points2[_dragIndex] = point;
			_dots.Draw ();
		}
	}
}
