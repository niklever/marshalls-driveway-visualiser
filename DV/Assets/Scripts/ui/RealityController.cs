﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class RealityController : MonoBehaviour {
	Image _sent;
	InputField _emailTxt;
	GameObject _emailGO;
	GameObject _emailLabel;

	public void Init(){
		GameObject go = transform.Find ("sent").gameObject;
		_sent = go.GetComponent<Image>();
		_sent.enabled = false;
		_emailLabel = transform.Find ("Text").gameObject;
		_emailGO = transform.Find ("EmailTxt").gameObject;
		_emailTxt = _emailGO.GetComponent<InputField>();
	}

	public void Show(){
		gameObject.SetActive(true);
		_emailGO.SetActive (true);
		_emailLabel.SetActive (true);
		_sent.enabled = false;
	}

	public void ClosePressed(){
		DV_UI.CloseModalPanel();
	}

	public void InstallerPressed(){
		DV_UI.ShowModalPanel("Installer");
	}

	public void StockistPressed(){
		DV_UI.ShowModalPanel("Stockist");
	}

	public void PDFPressed(){
		DVWebService.SendPDF(_emailTxt.text);
		_emailGO.SetActive (false);
		_emailLabel.SetActive (false);
		_sent.enabled = true;
	}
}
