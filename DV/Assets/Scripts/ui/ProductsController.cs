﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using System;
using System.Xml;
using System.Text;

public class ProductsController : MonoBehaviour {
	public GameObject _crumbButton;

	XmlDocument _menu;
	GameObject _crumbTrail;
	List<ProductIcon> _iconData;
	int _firstIcon;
	string _type;
	string _panelType;
	GameObject _swatchPanel;
	GameObject _heroPanel;
	Transform _pagePanel;
	Stack<XmlNode> _crumbTrailStack;
	float _crumbTrailLeft;
	int _page;
	string _title;

	// Use this for initialization
	void Start () {
		_crumbTrail = transform.Find ("CrumbtrailPanel").gameObject;
		_crumbTrail.SetActive (false);

		_swatchPanel = transform.Find ("Buttons").gameObject;
		_heroPanel = transform.Find ("HeroPanel").gameObject;
		_heroPanel.SetActive (false);

		_pagePanel = transform.Find ("PagePanel");
		_pagePanel.gameObject.SetActive (false);

		TextAsset data= Resources.Load<TextAsset>("Data/product-menu-patterns");
		_menu = new XmlDocument();
		_menu.LoadXml(data.text);

		_crumbTrailStack = new Stack<XmlNode>();

		if (_type!=null) Show(_type);

		_page = 0;
	}

	void UpdatePopular(int page){
		List<XmlNode> nodes = GetPopular ();
		bool last = false;
		if (page == 100) {
			//Next page
			page = _page + 1;
		}else if (page==99){
			//Previous page
			page = _page - 1;
		} else {
			last = (page == Math.Ceiling ((double)nodes.Count / 4.0));
		}
		_page = page;
		last = (page == Math.Ceiling ((double)nodes.Count / 4.0));
		_pagePanel.gameObject.SetActive (true);
		for(int i=1; i<_pagePanel.childCount-1; i++){
			GameObject go = _pagePanel.GetChild (i).gameObject;
			if (!go.activeSelf)
				break;
			Image image = go.GetComponent<Image>();
			if (i==page){
				image.sprite = Resources.Load<Sprite>("Textures/page_selected");
			}else{
				image.sprite = Resources.Load<Sprite>("Textures/page_unselected");
			}
		}
		Transform buttons = _heroPanel.transform;

		_pagePanel.GetChild (0).gameObject.SetActive (page > 1);
		_pagePanel.GetChild (_pagePanel.childCount - 1).gameObject.SetActive (!last);

		for (int i = 2; i < buttons.childCount; i++) {
			GameObject go = buttons.GetChild (i).gameObject;
			PavingButtonController controller = go.GetComponent<PavingButtonController> ();
			int idx = (page - 1) * 4 + (i - 2);
			if (idx<nodes.Count){
				XmlNode node = nodes [idx];
				ProductIcon icon = new ProductIcon (node);
				controller.UpdateButton (icon);
				go.SetActive(true);
			}else{
				go.SetActive(false);
			}
		}
	}

	// Update is called once per frame
	void UpdatePanel (string type, int first=0) {
		_firstIcon = first;
		_panelType = type;
		Transform buttons = null; 
		if (type=="swatch"){
			buttons = _swatchPanel.transform;
			_swatchPanel.SetActive (true);
			_heroPanel.SetActive (false);
		}else if (type=="hero"){
			buttons = _heroPanel.transform;
			_swatchPanel.SetActive (false);
			_heroPanel.SetActive (true);
		}

		for(int i=0; i<buttons.childCount; i++){
			GameObject button = buttons.GetChild (i).gameObject;
			if (_iconData!=null && (first + i)<_iconData.Count){
				PavingButtonController pbc = button.GetComponent<PavingButtonController>();
				if (pbc!=null) pbc.UpdateButton(_iconData[first + i]);
				button.SetActive (true);
			}else{
				button.SetActive(false);
			}
		}

		if (_iconData == null) {
			_pagePanel.gameObject.SetActive (false);
		}else if (type == "hero"){
			//Update Page Control
			List<XmlNode>nodes = GetPopular();
			int count = (int)Math.Ceiling((double)(nodes.Count/4.0));
			int current = 0;
			if (count>1){
				_pagePanel.gameObject.SetActive (true);
				for (int i = 0; i < _pagePanel.childCount-1; i++) {
					GameObject go = _pagePanel.GetChild (i).gameObject;
					go.SetActive (i <= count);
				}
				UpdatePopular(1);
			}else{
				_pagePanel.gameObject.SetActive (false);
			}
		}else{
			//Update Page Control
			int count = (int)Math.Ceiling(_iconData.Count/8.0);
			int current = (int)Math.Floor (first/8.0);
			if (count>1){
				_pagePanel.gameObject.SetActive (true);
				for(int i=0; i<_pagePanel.childCount; i++){
					GameObject go = _pagePanel.GetChild (i).gameObject;
					go.SetActive (i<count);
					Image image = go.GetComponent<Image>();
					if (i==current){
						image.sprite = Resources.Load<Sprite>("Textures/page_selected");
					}else{
						image.sprite = Resources.Load<Sprite>("Textures/page_unselected");
					}
				}
			}else{
				_pagePanel.gameObject.SetActive (false);
			}
		}
	}

	void AddXmlAttribute(XmlNode node, string name, string value){
		//node = node.PreviousSibling;
		string str = GetXmlAttribute (node, name);
		XmlAttribute attr;
		if (str == "") {
			XmlDocument doc = node.OwnerDocument;
			attr = doc.CreateAttribute (name);
			attr.Value = value;
			node.Attributes.Append (attr);
		} else {
			attr = node.Attributes[name];
			attr.Value = value;
		}
	}

	List<ProductIcon> GetPage(XmlNode node, string type){
		List<ProductIcon> icons = new List<ProductIcon>();
		if (_menu!=null){
			XmlNodeList pages = _menu.GetElementsByTagName("page");
			for(int i=0; i<pages.Count; i++){
				XmlNode page = pages[i];
				string pagetype = GetXmlAttribute(page, "producttypes");
				if (pagetype==type){
					for(int j=0; j<page.ChildNodes.Count; j++){
						ProductIcon icon = new ProductIcon(page.ChildNodes[j]);
						icons.Add (icon);
					}
					break;
				}
			}
		}
		return icons;
	}
	
	public void Show(string type){
		Debug.Log (string.Format ("ProductsController type:{0}", type));
		gameObject.SetActive(true);
		_type = type;
		if (_menu==null) return;
		switch(type){
		case "driveway":
		case "paving":
			_iconData = GetPage(_menu.FirstChild, "driveway");
			CreateCrumbTrail("Driveway");
			UpdatePanel("swatch", 0);
			break;
		case "edging":
		case "kerb":
			_iconData = GetPage(_menu.FirstChild, "edging,kerb");
			UpdatePanel("hero", 0);
			CreateCrumbTrail("Edging and Kerbs");
			break;
		case "features":
			_iconData = GetPage(_menu.FirstChild, "circle,octant");
			UpdatePanel("swatch", 0);
			CreateCrumbTrail("Circles and Octants");
			break;
		case "accessories":
			_iconData = GetPage(_menu.FirstChild, "accessory");
			UpdatePanel("swatch", 0);
			CreateCrumbTrail("Accessories");
			break;
		}
	}
		
	public void Show(XmlNode node, int hideCrumb=-1, bool fullcrumb=false){
		List<ProductIcon> icons = new List<ProductIcon>();

		XmlNode options = null;
		for(int i=0; i<node.ChildNodes.Count; i++){
			if (node.ChildNodes[i].Name=="page") options = node.ChildNodes[i];
		}

		try{
			Debug.Log ("ProductsController.Show " + options.InnerXml);
		}catch(NullReferenceException e){
			Debug.Log ("ProductsController.Show NullReferenceException");
		}
		for(int j=0; j<options.ChildNodes.Count; j++){
			ProductIcon icon = new ProductIcon(options.ChildNodes[j]);
			icons.Add (icon);
		}
		_iconData = icons;
		UpdatePanel("swatch", 0);
		if (hideCrumb==-1){
			AppendCrumbTrail(GetXmlAttribute(node, "name"), node);
		}else{
			Transform panel = _crumbTrail.transform.Find ("Panel");
			for(int i=panel.childCount-1; i>=hideCrumb; i--){
				GameObject go = panel.GetChild (i).gameObject;
				go.transform.parent = null;
				Destroy(go);
			}
			GameObject button = panel.GetChild (panel.childCount-1).gameObject;
			CrumbButtonController controller = button.GetComponent<CrumbButtonController>();
			_crumbTrailLeft = controller.Right;
		}
	}

	void CreateCrumbTrail(string name){
		int left = 10;
		Transform panel = _crumbTrail.transform.Find ("Panel");
		for(int i=panel.childCount-1; i>=0; i--){
			GameObject go = panel.GetChild (i).gameObject;
			go.transform.parent = null;
			Destroy(go);
		}
		_crumbTrail.SetActive(true);
		GameObject button = Instantiate(_crumbButton);
		button.transform.SetParent(panel, false);
		CrumbButtonController controller = button.GetComponent<CrumbButtonController>();
		controller.UpdateButton(name, left, _type);
		_crumbTrailLeft = controller.Right;
		Debug.Log (string.Format("ProductsController.CreateCrumbTrail left:{0} name:{1}", _crumbTrailLeft, name));
	}

	void AppendCrumbTrail(string name, XmlNode node){
		Transform panel = _crumbTrail.transform.Find ("Panel");
		GameObject button = Instantiate(_crumbButton);
		button.transform.SetParent(panel, false);
		CrumbButtonController controller = button.GetComponent<CrumbButtonController>();
		controller.UpdateButton(name, _crumbTrailLeft, node);
		_crumbTrailLeft = controller.Right;
		Debug.Log (string.Format("ProductsController.AppendCrumbTrail left:{0} name:{1}", _crumbTrailLeft, name));
	}

	public void ChangePagePressed(int page){
		UpdatePanel(_panelType, (page-1)*8);
	}

	public void ClosePressed(){
		DV_UI.CloseModalPanel ();
	}
	
	string GetXmlAttribute(XmlNode node, string str){
		XmlAttribute attr = node.Attributes[str];
		if (attr==null) return "";
		return attr.Value;
	}

	List<XmlNode> GetNodes(string nodeName, string attributeName, string attributeValue){
		List<XmlNode> nodes = new List<XmlNode>();
		if (_menu!=null){
			XmlNodeList pages = _menu.GetElementsByTagName(nodeName);
			for(int i=0; i<pages.Count; i++){
				XmlNode page = pages[i];
				string attr = GetXmlAttribute(page, attributeName);
				if (attr==attributeValue){
					nodes.Add (page);
					break;
				}
			}
		}
		return nodes;
	}

	List<XmlNode> GetPopular(){
		List<XmlNode> lst = new List<XmlNode> ();
		if (_menu!=null){
			XmlNodeList pages = _menu.GetElementsByTagName("option");
			for(int i=0; i<pages.Count; i++){
				XmlNode page = pages[i];
				string popular = GetXmlAttribute(page, "popular");
				if (popular=="true") lst.Add (page);
			}
		}
		return lst;
	}

	string DetailsToString(Dictionary<string,string>details){
		StringBuilder str = new StringBuilder();
		foreach (string key in details.Keys) {
			str.Append(string.Format ("{0}:{1} ", key, details[key]));
		}
		return str.ToString();
	}

	public Dictionary<string, string> GetDictionaryFromXml(XmlNode node){
		Dictionary<string, string> details = new Dictionary<string, string>();
		StringBuilder str = new StringBuilder();

		if (node!=null){
			bool found = false;
			int count = 0;

			do{
				if (node.Name=="productproperties"){
					foreach(XmlAttribute attr in node.Attributes){
						try{
							details.Add (attr.Name, attr.Value);
							str.Append(string.Format ("{0}:{1} ", attr.Name, attr.Value));
						}catch(ArgumentException e){
							Debug.Log (string.Format("ProductsController.GetDictionaryFromXml {0}:{1}", attr.Name, attr.Value));
						}
					}
				}
				while(node!=null && node.Name!="option") node = node.ParentNode;
				found = false;
				count++;
			}while(node!=null && count<20);
			//Debug.Log ("GardenScript.GetDictionaryFromXml " + str);
		}

		return details;
	}

	void CheckParentAttributes(XmlAttribute attr, XmlNode parent, Dictionary<string,string>details){
		XmlNode parentAttr;
		switch(attr.Name){
		case "color":
			parentAttr = parent.Attributes.GetNamedItem("name");
			if (parentAttr!=null && !details.ContainsKey("displaycolor")){
				details.Add ("displaycolor", parentAttr.Value);
			}
			parentAttr = parent.Attributes.GetNamedItem("image");
			if (parentAttr!=null && !details.ContainsKey("colorswatch")){
				details.Add ("colorswatch", parentAttr.Value);
			}
			break;
		case "pattern":
			parentAttr = parent.Attributes.GetNamedItem("name");
			if (parentAttr!=null && !details.ContainsKey("displaypattern")){
				details.Add ("displaypattern", parentAttr.Value);
			}
			parentAttr = parent.Attributes.GetNamedItem("image");
			if (parentAttr!=null && !details.ContainsKey("patternswatch")){
				details.Add ("patternswatch", parentAttr.Value);
			}
			break;
		}
	}

	public void Swipe(bool left){
		if (left && _firstIcon>0){
			_firstIcon-=8;
			UpdatePanel(_type, _firstIcon);
		}else if (!left && (_firstIcon+8)<_iconData.Count){
			_firstIcon+=8;
			UpdatePanel(_type, _firstIcon);
		}
	}
}
