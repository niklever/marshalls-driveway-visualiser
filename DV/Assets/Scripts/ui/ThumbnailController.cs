﻿using UnityEngine;
using UnityEngine.UI;
using System;
using System.Collections;
using SimpleJSON;

public class ThumbnailController : MonoBehaviour {
	static ProjectsController _projectsController;

	Image _image;
	Image _tick;
	Image _loader;
	RectTransform _loaderRT;
	Text _name;
	Text _date;
	bool _loading;
	DVWebService _ws;
	string _guid;
	public string Guid{
		get{
			return _guid;
		}
	}
	public string Base64{
		get{
			return System.Convert.ToBase64String(((Texture2D)_image.sprite.texture).EncodeToPNG());
		}
	}
	bool _edit;
	public bool Selected{
		get{
			return _tick.enabled;
		}
	}

	public Texture2D Thumbnail{
		set{
			_image.sprite = Sprite.Create ( value, new Rect(0, 0, value.width, value.height), Vector2.zero);
			ShowLoading(false);
		}
	}

	// Use this for initialization
	void Start () {
		if (_projectsController==null){
			GameObject go = GameObject.Find ("Canvas/ProjectsPanel");
			_projectsController = go.GetComponent<ProjectsController>();
		}
	}

	void Init(){
		GameObject go = transform.Find ("Image").gameObject;
		_image = go.GetComponent<Image>();
		go = transform.Find ("Tick").gameObject;
		_tick = go.GetComponent<Image>();
		go = transform.Find ("Loader").gameObject;
		_loader = go.GetComponent<Image>();
		_loaderRT = go.GetComponent<RectTransform>();
		go = transform.Find ("Name").gameObject;
		_name = go.GetComponent<Text>();
		go = transform.Find ("Date").gameObject;
		_date = go.GetComponent<Text>();
		_edit = false;
	}

	void Update(){
		if (_loading){
			Vector3 rot = _loaderRT.rotation.eulerAngles;
			int segment = (int)((Time.time * 12) % 12);
			rot.z = 360 - (segment * 30);
			Quaternion rotQ = Quaternion.identity;
			rotQ.eulerAngles = rot;
			_loaderRT.rotation = rotQ;
		}
	}

	public void Set (JSONNode data) {
		if (_ws==null) _ws = new DVWebService();
		Init();
		//Debug.Log ("ThumbnailController.Set " + data.ToString ());
		_name.text = data["Name"];
		DateTime date = DateTime.Parse(data["CreatedDate"]);
		_date.text = date.ToString("dd/MM/yy");
		_ws.GetThumbnail(data["ThumbnailImageUrl"], this);
		_guid = data["Guid"];
		ShowLoading(true);
	}

	public void ShowLoading(bool show){
		_loader.enabled = show;
		_loading = show;
	}

	public void SetEditState(bool mode, bool updateEditState=true){
		if (mode){
			if (_image) _image.color = new Color(1,1,1,0.5f);
			if (_tick && updateEditState) _tick.enabled = false;
			if (_name) _name.color = new Color(1,1,1,0.5f);
			if (_date) _date.color = new Color(1,1,1,0.5f);
		}else{
			if (_image) _image.color = new Color(1,1,1,1);
			if (_tick && updateEditState) _tick.enabled = false;
			if (_name) _name.color = new Color(1,1,1,1);
			if (_date) _date.color = new Color(1,1,1,1);
		}
		if (updateEditState) _edit = mode;
	}

	public void WSError(string str){
		ShowLoading(false);
		Debug.Log ("ThumbnailController.WSError " + str);
	}

	public void ThumbnailPressed(){
		Debug.Log ("ThumbnailController.ThumbnailPressed " + _guid);
		if (_edit){
			if (_tick) _tick.enabled = !_tick.enabled;
			SetEditState(!_tick.enabled, false);
			if (_projectsController!=null) _projectsController.UpdateEditHeader();
		}else{
			DVWebService.GetProject(_guid);
		}
	}
}
