﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class DV_UI : MonoBehaviour {
	static GameObject _modalPanels;
	static Animator _animator;
	GameObject _busy;
	GameObject _alertPanel;
	static GameObject _canvasBehind;
	public static GameObject CanvasBehind{
		get{
			return _canvasBehind;
		}
	}
	static Image _projectImage;
	public static Image ProjectImage{
		get{
			return _projectImage;
		}
	}
	public static bool ModalActive{
		get{
			return (_activeModalPanel!=null && _activeModalPanel.Count>0);
		}
	}
	static Image _bgImage;

	static NewProjectController _newProjectController;
	static PhotoController _photoController;
	static ProjectsController _projectsController;
	static DesignController _designController;
	public static DesignController DesignController{
		get{
			return _designController;
		}
	}
	static AlertController _alertController;
	static ProductsController _productsController;
	static RealityController _realityController;
	static InstallerStockistController _installerStockistController;
	static ScreenshotsController _screenshotsController;
	static SettingsController _settingsController;
	static HelpController _helpController;
	static DrawShape _drawShape;
	Vector3 _imagePos;
	Quaternion _imageRot;
	Vector3 _imageScale;

	public void ResetImage(){
		Transform t = GameObject.Find ("CanvasBehind3D/Image").transform;
		t.position = _imagePos;
		t.rotation = _imageRot;
		t.localScale = _imageScale;
		Image image = t.gameObject.GetComponent<Image> ();
		image.color = new Color (1, 1, 1, 0);
	}

	public static DrawShape DrawShape{
		get{
			return _drawShape;
		}
		set{
			_drawShape = value;
		}
	}
	public static bool ButtonCapturedMouse = false;
	public static bool Change{ get; set; }

	static Stack<GameObject> _activeModalPanel;
	DrivewayController _drivewayController;
	public DrivewayController DrivewayController{
		get{
			if (_drivewayController==null){
				GameObject go = GameObject.Find ("3D");
				_drivewayController = go.GetComponent<DrivewayController>();
			}
			return _drivewayController;
		}
	}

	public static bool DesignActive{
		get{
			bool active = false;
			if (_designController!=null) active = _designController.gameObject.activeSelf;
			return active;
		}
	}

	// Use this for initialization
	void Start () {
		_animator = GetComponent<Animator>();
		_busy = GameObject.Find ("Canvas/BusyPanel");
		_busy.SetActive (false);
		GameObject go = GameObject.Find ("Canvas/ModalPanels/NewProjectPanel");
		_newProjectController = go.GetComponent<NewProjectController> ();
		go.SetActive (false);
		if (_modalPanels==null) _modalPanels = GameObject.Find ("Canvas/ModalPanels");
		go = GameObject.Find ("Canvas/AlertPanel");
		if (_alertController==null) _alertController = go.GetComponent<AlertController>();
		go = GameObject.Find ("Canvas/ModalPanels/PhotoPanel");
		_photoController = go.GetComponent<PhotoController>();
		go.SetActive (false);
		go = GameObject.Find ("Canvas/ModalPanels/ProductsPanel");
		_productsController = go.GetComponent<ProductsController>();
		go.SetActive (false);
		go = GameObject.Find ("Canvas/ModalPanels/RealityPanel");
		_realityController = go.GetComponent<RealityController>();
		_realityController.Init ();
		go.SetActive (false);
		go = GameObject.Find ("Canvas/ModalPanels/InstallerStockistPanel");
		_installerStockistController = go.GetComponent<InstallerStockistController>();
		_installerStockistController.Init();
		go.SetActive (false);
		go = GameObject.Find ("Canvas/ModalPanels/ScreenshotsPanel");
		_screenshotsController = go.GetComponent<ScreenshotsController>();
		go.SetActive (false);
		go = GameObject.Find ("Canvas/ModalPanels/SettingsPanel");
		_settingsController = go.GetComponent<SettingsController>();
		_settingsController.Init ();
		go.SetActive (false);
		go = GameObject.Find ("Canvas/ModalPanels/HelpPanel");
		_helpController = go.GetComponent<HelpController>();
		go.SetActive (false);
		_modalPanels.SetActive (false);
		go = GameObject.Find ("Canvas/DesignPanel");
		_designController = go.GetComponent<DesignController>();
		go.SetActive(false);
		_alertController.Hide ();
		_activeModalPanel = new Stack<GameObject>();
		_canvasBehind = GameObject.Find ("CanvasBehind3D");
		go = GameObject.Find ("CanvasBehind3D/Image"); 
		_imagePos = go.transform.position;
		_imageRot = go.transform.rotation;
		_imageScale = go.transform.localScale;
		go = GameObject.Find ("CanvasBg/Image"); 
		_bgImage = go.GetComponent<Image> ();
		_bgImage.color = new Color (1, 1, 1, 0);

		_projectImage = go.GetComponent<Image>();
		_canvasBehind.SetActive (false);
	}
	
	public static void Animate(string trigger){
		_animator.SetTrigger (trigger);
	}

	public bool Busy{
		set{
			_busy.SetActive (value);
		}
	}

	public static void ShowProjectsPanel(bool show){
		if (_projectsController!=null) _projectsController.gameObject.SetActive (show);
		if (show) Animate("designOut");
	}

	public static void ShowDesignPanel(bool show){
		if (_designController!=null) _designController.gameObject.SetActive (show);
		if (show) Animate("designIn");
	}

	public static void ShowError(string message){
		Debug.Log (string.Format ("DV_UI.ShowError message:{0}", message));
		if (_modalPanels) _modalPanels.SetActive (true);
		if (_alertController!=null) _alertController.Show ("ERROR", message);
	}
	
	public static void ShowAlert(string title, string message){
		Debug.Log (string.Format ("DV_UI.ShowError title:{0} message:{1}", title, message));
		if (_modalPanels) _modalPanels.SetActive (true);
		if (_alertController!=null) _alertController.Show (title, message);
	}

	public void CloseAlert(){
		if (_modalPanels){
			bool activePanel = false;
			for(int i=0; i<_modalPanels.transform.childCount; i++){
				GameObject go = _modalPanels.transform.GetChild (i).gameObject;
				if (go.activeSelf){
					activePanel = true;
					break;
				}
			}
			_modalPanels.SetActive (activePanel);
		}
		if (_alertController!=null) _alertController.Hide ();
	}

	public static void ShowModalPanel(string panel, string type="", bool change=false){
		Change = change;
		_modalPanels.SetActive(true);
		switch(panel){
		case "NewProject":
			_newProjectController.Show(true);
			_newProjectController.Clear ();
			_activeModalPanel.Push(_newProjectController.gameObject);
			break;
		case "Draw":
			_photoController.Show(true);
			_activeModalPanel.Push(_photoController.gameObject);
			break;
		case "Photo":
			_photoController.Show(false);
			_activeModalPanel.Push(_photoController.gameObject);
			break;
		case "Products":
			_productsController.Show(type);
			_activeModalPanel.Push(_productsController.gameObject);
			break;
		case "Reality":
			_realityController.Show();
			_activeModalPanel.Push(_realityController.gameObject);
			break;
		case "Installer":
			_installerStockistController.Show (true);
			_activeModalPanel.Push(_installerStockistController.gameObject);
			break;
		case "Stockist":
			_installerStockistController.Show (false);
			_activeModalPanel.Push(_installerStockistController.gameObject);
			break;
		case "Screenshots":
			_screenshotsController.Show ();
			_activeModalPanel.Push(_screenshotsController.gameObject);
			break;
		case "Settings":
			_settingsController.Show();
			_activeModalPanel.Push(_settingsController.gameObject);
			break;
		case "Help":
			_helpController.Show();
			_activeModalPanel.Push(_helpController.gameObject);
			break;
		default:
			Debug.Log ( string.Format("DV_UI.ShowModalPanel {0} not supported", panel));
			_modalPanels.SetActive(false);
			break;
		}
	}

	public void CloseNewProjectPanel(){
		DV_UI.CloseModalPanel ();
	}

	public static void CloseModalPanel(){
		Debug.Log ("DV_UI.CloseModalPanel " + _activeModalPanel.Count);
		if (_activeModalPanel != null && _activeModalPanel.Count > 0) {
			GameObject panel = _activeModalPanel.Pop ();
			_modalPanels.SetActive (_activeModalPanel.Count > 0);
			panel.SetActive (false);
		} else {
			_modalPanels.SetActive (false);
		}
	}

	public void NewProjectPressed(){
		Debug.Log ("DV_UI.NewProjectPressed");
		string name = NewProjectController.Name;
		if (name==""){
			ShowAlert("New project error", "Please enter a project name");
		}else{
			if (DrivewayController) _drivewayController.CreateNewProject(name);
		}
	}

	public void DrawOutline(List<Vector2> vertices, float lineWidth=5.0f, bool flipY=true){
		_canvasBehind.SetActive (true);
		Transform parent = _canvasBehind.transform.Find ("Image");
		//Clear
		while(parent.childCount>0){
			Transform t = parent.GetChild(0);
			t.SetParent( null, false);
			Destroy(t.gameObject);
		}
		float offsetY = 0;
		Vector3 pointA = Vector3.zero;
		bool first = true;

		foreach(Vector3 vertex in vertices){
			if (first){
				first = false;
			}else{
				Vector3 pointB = vertex;
				pointB.y = 1590 - pointB.y + offsetY;
				Vector3 delta = pointB - pointA;
				GameObject image = new GameObject();
				image.AddComponent<Image>();
				RectTransform rt = image.GetComponent<RectTransform>();
				rt.sizeDelta = new Vector2( delta.magnitude, lineWidth);
				rt.pivot = new Vector2(0, 0.0f);
				rt.anchorMax = rt.pivot;
				rt.anchorMin = rt.pivot;
				//pointA.z = parent.position.z;
				rt.position = pointA;
				float angle = Mathf.Atan2(delta.y, delta.x) * Mathf.Rad2Deg;
				rt.rotation = Quaternion.Euler(0,0, angle);
				image.transform.SetParent (parent, false);
				//image.transform.SetParent (parent, true);
			}
			pointA = vertex;
			pointA.y = 1590 - pointA.y + offsetY;
		}
	}

	public void ObjectSelected(bool selected, string type="driveway")
	{
		if (_designController != null){
			if (selected){
				switch(type){
				case "driveway":
					_designController.SetFooterState("paving_selected");
					break;
				case "kerb":
				case "edging":
					_designController.SetFooterState("kerb_selected");
					break;
				case "circle":
				case "octant":
				case "square":
				case "feature":
					_designController.SetFooterState("feature_selected");
					break;
				case "accessory":
					_designController.SetFooterState("accessory_selected");
					break;
				}
			}else{
				_designController.SetFooterState("edit");
			}
		}
	}

	public static void NewProjectImage(Texture2D tex){
		GameObject go;
		Image image = null;
		if (_canvasBehind!=null){
			_canvasBehind.SetActive (true);
			go = _canvasBehind.transform.GetChild (0).gameObject;
			image = go.GetComponent<Image>();
			image.sprite = Sprite.Create (tex, new Rect(0,0,tex.width,tex.height), Vector2.zero);
			image.color = new Color (1, 1, 1, 1);
		}
		go = GameObject.Find("CanvasThumbnail/Image");
		if (go!=null && image!=null){
			Image image1 = go.GetComponent<Image>();
			image1.sprite = image.sprite;
		}
		if (_bgImage != null) {
			_bgImage.sprite = image.sprite;
			_bgImage.color = new Color (1, 1, 1, 1);
		}
	}

	public static void SetDefaultHouse(){
		GameObject go = _canvasBehind.transform.GetChild (0).gameObject;
		Image image = go.GetComponent<Image>();
		image.sprite = Resources.Load<Sprite>("Textures/photo_house_default");
		image.color = new Color (1, 1, 1, 1);
	}
}
