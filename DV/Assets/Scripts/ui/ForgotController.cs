﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections;

public class ForgotController : MonoBehaviour {
	DV_UI _dv_ui;
	EventSystem _es;

	public string UserName;

	// Use this for initialization
	void Start () {
		GameObject go = GameObject.Find ("Canvas");
		_dv_ui = go.GetComponent<DV_UI>();
		_es = EventSystem.current;
	}

	public void Update()
	{

		if (Input.GetKeyDown(KeyCode.Tab))
		{
			Selectable next = _es.currentSelectedGameObject.GetComponent<Selectable>().FindSelectableOnDown();

			if (next!= null) {

				InputField inputfield = next.GetComponent<InputField>();
				if (inputfield !=null) inputfield.OnPointerClick(new PointerEventData(_es));  //if it's an input field, also set the text caret

				_es.SetSelectedGameObject(next.gameObject, new BaseEventData(_es));
			}

		}
	}

	public void BackPressed(){
		DV_UI.Animate ("loginIn");
	}

	public void ForgotPressed(){
		Transform t = transform.Find ("EmailTxt");
		UserName = "";
		if (t!=null){
			GameObject go = t.gameObject;
			if (go) {
				InputField ip = t.gameObject.GetComponent<InputField> ();
				UserName = ip.text;
			}
		}
		DVWebService.ForgottenPassword (this);
	}

	public void ForgottenPasswordSuccess(){
		DV_UI.ShowAlert("Forgotten password", "A reminder has been sent to your email address");
		DV_UI.Animate ("loginIn");
	}
}
