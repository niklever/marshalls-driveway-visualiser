﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Xml;

public enum CrumbButtonType{ NORMAL, PENULTIMATE, LAST };

public class CrumbButtonController : MonoBehaviour {
	static ProductsController _productsController;

	XmlNode _node;
	int _childPosition;
	float _textWidth;
	string _type;
	string _name;

	public float Right{
		get{
			RectTransform rt = GetComponent<RectTransform>();
			return rt.anchoredPosition.x + _textWidth + 20;
		}
	}

	public void UpdateButton(string name, float left, string type){
		_type = type;
		UpdateButton(name, left);
	}

	// Update is called once per frame
	public void UpdateButton (string name, float left, XmlNode node=null, CrumbButtonType type = CrumbButtonType.NORMAL) {
		RectTransform rt = GetComponent<RectTransform>();
		Vector2 pos = rt.anchoredPosition;
		pos.x = left;
		rt.anchoredPosition = pos;
		GameObject go = transform.Find ("Text").gameObject;
		Text txt = go.GetComponent<Text>();
		txt.text = string.Format ("{0} >", name);
		_textWidth = txt.preferredWidth;
		_name = name;
		UpdateButton (type);
		_node = node;
		go = GameObject.Find ("Canvas/ModalPanels/ProductsPanel/CrumbtrailPanel/Panel");
		_childPosition = go.transform.childCount;
	}

	public void UpdateButton(CrumbButtonType type, Text txt=null){
		if (txt==null){
			GameObject go = transform.Find ("Text").gameObject;
			txt = go.GetComponent<Text>();
		}
		switch (type) {
		case CrumbButtonType.NORMAL:
			txt.text = string.Format ("{0} :", _name);
			txt.color = new Color (54/255.0f, 56/255.0f, 56/255.0f);
			break;
		case CrumbButtonType.PENULTIMATE:
			txt.text = string.Format ("{0} >", _name);
			txt.color = new Color (54/255.0f, 56/255.0f, 56/255.0f);
			break;
		case CrumbButtonType.LAST:
			txt.text = string.Format ("{0} ", _name);
			txt.color = new Color (109 / 255.0f, 112 / 255.0f, 112 / 255.0f);
			break;
		}
	}

	public void ButtonPressed(){
		if (_node!=null){
			Debug.Log(string.Format("CrumbButtonController.ButtonPressed pos:{0} node:{1}", _childPosition, _node.InnerXml));
		}else{
			Debug.Log(string.Format("CrumbButtonController.ButtonPressed pos:{0}", _childPosition));
		}
		if (_productsController==null){
			GameObject go = GameObject.Find ("Canvas/ModalPanels/ProductsPanel");
			_productsController = go.GetComponent<ProductsController>();
		}
		if (_childPosition==1){
			_productsController.Show (_type);
		}else{
			_productsController.Show (_node, _childPosition);
		}
	}
}
