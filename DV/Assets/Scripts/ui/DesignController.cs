﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

enum FooterButtons { CHANGE, FEATURES, KERBS, ACCESSORIES, RESTART, UNDO, ROTATE_PHOTO, ROTATE, POSITION, CHANGE_KERB, CHANGE_ACCESSORY, CHANGE_FEATURE, POSITION_PHOTO, SCALE_PHOTO, TILT_PLANE, MOVE_PLANE, ROTATE_PLANE, PERSPECTIVE, PHOTO };

public class DesignController : MonoBehaviour {
	string _editState;
	DrivewayController _drivewayController;
	DV_UI _dv_ui;
	GameObject _snapshotsPanel;
	GameObject _bg;
	public GameObject Bg;
	GameObject _deleteBtn;
	GameObject _deleteConfirmText;
	GameObject _deleteConfirmBtn;
	GameObject _selected;
	GameObject _threeD;
	Vector3 _imagePos;
	Quaternion _imageRot;
	Vector3 _imageScale;
	string _type;

	// Use this for initialization
	void Start () {
		GameObject go = GameObject.Find ("Canvas");
		_dv_ui = go.GetComponent<DV_UI>();
		_snapshotsPanel = transform.Find ("SnapshotsPanel").gameObject;
		_snapshotsPanel.SetActive(false);
		SetFooterState("edit");
		_bg = GameObject.Find ("Canvas/Image");
		_deleteBtn = GameObject.Find ("Canvas/DesignPanel/Footer/DeleteButton/DeleteButton");
		_deleteConfirmText = GameObject.Find ("Canvas/DesignPanel/Footer/DeleteButton/Text");
		_deleteConfirmBtn = GameObject.Find ("Canvas/DesignPanel/Footer/DeleteButton/ConfirmButton");
		SetDeleteBtn(false);
		_threeD = GameObject.Find ("3D");
		_drivewayController = _threeD.GetComponent<DrivewayController>(); 
		_selected = GameObject.Find ("Canvas/DesignPanel/Footer/Buttons/Selected");
		_selected.SetActive (false);
	}

	void SetDeleteBtn(bool confirm){
		if (_deleteBtn) _deleteBtn.SetActive (!confirm);
		if (_deleteConfirmText) _deleteConfirmText.SetActive (confirm);
		if (_deleteConfirmBtn) _deleteConfirmBtn.SetActive (confirm);
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void DeletePressed(){
		Debug.Log ("DesignController.DeletePressed");
		SetDeleteBtn(true);
	}

	public void DeleteConfirmPressed(){
		Debug.Log ("DesignController.DeleteConfirmPressed");
		_drivewayController.DeleteSelected();
		SetFooterState("edit");
	}

	void EnableThreeDChildren(bool show){
		for(int i=0; i<_threeD.transform.childCount; i++){
			Transform t = _threeD.transform.GetChild(i);
			if (t.gameObject != null) t.gameObject.SetActive(show);
		}
	}

	public void CancelPressed(){
		Debug.Log ("DesignController.CancelPressed");
		DV_UI.DrawShape.enabled = false;
		int previousMode = _drivewayController.MouseMode;
		_drivewayController.MouseMode = (int)MouseModes._NONE;
		Product driveway = DrivewayController.Project.Driveway;
		switch(_editState){
		case "position_photo":
			Transform t = GameObject.Find ("CanvasBehind3D/Image").transform;
			t.position = _imagePos;
			t.rotation = _imageRot;
			t.localScale = _imageScale;
			if (driveway == null || driveway.SourceVertices2d.Count < 4) {
				_drivewayController.MouseMode = previousMode;
				return;
			} else {
				EnableThreeDChildren (true);
			}
			break;
		case "draw_paving":
			if (driveway == null || driveway.SourceVertices2d.Count < 4) {
				_drivewayController.MouseMode = previousMode;
				SetFooterState("position_photo");
				return;
			} else {
				ShowOutline(true);
			}
			break;
		case "tilt_plane":
			//TO DO restore plane to position before move
			_drivewayController.RestorePlaneTransform();
			_drivewayController.ShowPlane(false);
			if (driveway == null || driveway.SourceVertices2d.Count < 4) {
				_drivewayController.MouseMode = previousMode;
				SetFooterState("position_photo");
				return;
			}
			break;
		}
		SetFooterState("edit");
	}

	public void PhotoPressed(){
		Debug.Log ("DesignController.PhotoPressed");
		DV_UI.DrawShape.enabled = false;
		DV_UI.ShowModalPanel("Photo");
		//SetFooterState("edit");
	}

	public void DonePressed(){
		Debug.Log ("DesignController.DonePressed " + _editState);
		int mode = _drivewayController.MouseMode;
		_drivewayController.MouseMode = (int)MouseModes._NONE;
		Product driveway = DrivewayController.Project.Driveway;
		switch(_editState){
		case "position_photo":
			Transform t = GameObject.Find("CanvasBehind3D/Image").transform;
			DrivewayController.Project.HousePosition = new Vector2(t.position.x, t.position.y);
			DrivewayController.Project.HouseRotation = t.rotation.eulerAngles.z;
			DrivewayController.Project.HouseScale = t.localScale.x;
			if (driveway==null || driveway.SourceVertices2d.Count<4){
				SetFooterState("draw_paving");
			}else{
				SetFooterState("edit");
			}
			EnableThreeDChildren(true);
			break;
		case "draw_paving":
			if (DV_UI.DrawShape.SourceVertices2d.Count < 4) {
				DV_UI.ShowError ("Please draw your driveway by clicking on the screen");
				_drivewayController.MouseMode = mode;
			} else {
				_threeD.SetActive (true);
				_drivewayController.UpdateDriveway (DV_UI.DrawShape.SourceVertices2d);
				DV_UI.DrawShape.Clear ();
				SetFooterState ("edit");
				DrivewayController.Modified = true;
			}
			break;
		case "tilt_plane":
			_drivewayController.ShowPlane(false);
			DrivewayController.Project.PlanePosition = _drivewayController.Plane.transform.position;
			DrivewayController.Project.PlaneRotation = _drivewayController.Plane.transform.rotation.eulerAngles;
			if (driveway==null || driveway.SourceVertices2d.Count<4){
				SetFooterState("draw_paving");
			}else{
				SetFooterState("edit");
				_threeD.SetActive (true);
				_drivewayController.Clear ();
				_drivewayController.Init (DrivewayController.Project);
				DrivewayController.Modified = true;
			}
			break;
		case "draw_kerb":
			_drivewayController.CreateEdging ();
			DV_UI.DrawShape.Clear ();
			DV_UI.DrawShape.enabled = false;
			Set3DActive (true);
			SetFooterState ("edit");
			break;
		}
	}

	public void BackPressed(){
		Debug.Log ("DesignController.BackPressed");
		_drivewayController.SaveOnBack ();

		if (_bg) _bg.SetActive (true);
	}

	public void Hide(){
		gameObject.SetActive (false);
	}

	public void ChangePressed(){
		Debug.Log ("DesignController.ChangePressed");
		//DV_UI.ShowModalPanel("Draw");
		SetFooterState("draw_paving");
	}

	public void SnapshotPressed(){
		Debug.Log ("DesignController.SnapshotPressed");
		_snapshotsPanel.SetActive(!_snapshotsPanel.activeSelf);
	}

	public void ActionPressed(){
		Debug.Log ("DesignController.ActionPressed");
		DV_UI.DrawShape.Clear();
		DV_UI.ShowModalPanel("Reality");
	}

	public void HelpPressed(){
		Debug.Log ("DesignController.HelpPressed");
		DV_UI.DrawShape.Clear();
	#if UNITY_EDITOR
		Application.OpenURL (string.Format ("{0}dv/help/index.php", DVWebService.ResourcePath));
	#else
		#if UNITY_WEBPLAYER
			Application.ExternalCall("showHelp", true);
		#else
		Application.OpenURL (string.Format ("{0}dv/help/index.php", DVWebService.ResourcePath));
		#endif
	#endif	
		Analytics.ga ("Help", "Open help");
	}

	public void SettingsPressed(){
		Debug.Log ("DesignController.SettingsPressed");
		DV_UI.DrawShape.Clear();
		DV_UI.ShowModalPanel("Settings");
	}

	public void TakeScreenshotPressed(){
		Debug.Log ("DesignController.TakeScreenshotPressed");
		_snapshotsPanel.SetActive (false);
	}

	public void ViewScreenshotsPressed(){
		Debug.Log ("DesignController.ViewScreenshotsPressed");
		_snapshotsPanel.SetActive (false);
		DV_UI.ShowModalPanel("Screenshots");
	}

	public void FooterChangePressed(){
		Debug.Log ("DesignController.FooterChangePressed");
		SetSelected(FooterButtons.CHANGE);
		_drivewayController.MouseMode = (int)MouseModes._NONE;
		//if (_drivewayController.Driveway!=null) _drivewayController.SelectedProduct = _drivewayController.Driveway.Product;
		DV_UI.ShowModalPanel("Products", _type, true);
		Analytics.ga ("Project", "Change", _type);
	}

	public void FooterFeaturesPressed(){
		Debug.Log ("DesignController.FooterFeaturesPressed");
		SetSelected(FooterButtons.FEATURES);
		_drivewayController.MouseMode = (int)MouseModes._NONE;
		DV_UI.ShowModalPanel("Products", "features");
	}

	public void FooterKerbsPressed(){
		Debug.Log ("DesignController.FooterKerbsPressed");
		SetSelected(FooterButtons.KERBS);
		_drivewayController.MouseMode = (int)MouseModes._NONE;
		DV_UI.ShowModalPanel("Products", "edging");
	}

	public void FooterAccessoriesPressed(){
		Debug.Log ("DesignController.FooterAccessoriesPressed");
		SetSelected(FooterButtons.ACCESSORIES);
		_drivewayController.MouseMode = (int)MouseModes._NONE;
		DV_UI.ShowModalPanel("Products", "accessories");
	}

	public void FooterRestartPressed(){
		Debug.Log ("DesignController.FooterRestartPressed");
		//SetSelected(FooterButtons.RESTART);
		//_drivewayController.MouseMode = (int)MouseModes._NONE;
		DV_UI.DrawShape.Restart();
	}

	public void FooterUndoPressed(){
		Debug.Log ("DesignController.FooterUndoPressed");
		//SetSelected(FooterButtons.UNDO);
		//_drivewayController.MouseMode = (int)MouseModes._NONE;
		DV_UI.DrawShape.Undo();
	}

	public void FooterRotatePhotoPressed(){
		Debug.Log ("DesignController.FooterRotatePhotoPressed");
		SetSelected(FooterButtons.ROTATE_PHOTO);
		_drivewayController.MouseMode = (int)MouseModes._ROTATE_PHOTO;
	}

	public void FooterRotatePressed(){
		Debug.Log ("DesignController.FooterRotatePressed");
		SetSelected(FooterButtons.ROTATE);
		if (_drivewayController.DrivewaySelected){
			_drivewayController.MouseMode = (int)MouseModes._ROTATE_PATTERN;
		}else{
			_drivewayController.MouseMode = (int)MouseModes._ROTATE;
		}
	}

	public void FooterPositionPressed(){
		Debug.Log ("DesignController.FooterPositionPressed");
		SetSelected(FooterButtons.POSITION);
		if (_drivewayController.DrivewaySelected){
			_drivewayController.MouseMode = (int)MouseModes._MOVE_PATTERN;
		}else{
			_drivewayController.MouseMode = (int)MouseModes._MOVE;
		}
	}

	public void FooterPositionPhotoPressed(bool checkState=true){
		Debug.Log ("DesignController.FooterPositionPhotoPressed");
		Product driveway = DrivewayController.Project.Driveway;
		SetSelected(FooterButtons.POSITION_PHOTO);
		if (_editState == "position_photo") {
			_drivewayController.MouseMode = (int)MouseModes._MOVE_PHOTO;
		}else if (checkState && (_editState=="draw_paving" || driveway==null || driveway.SourceVertices2d.Count<4)){
			SetFooterState("position_photo");
		}else{
			_drivewayController.MouseMode = (int)MouseModes._MOVE_PHOTO;
		}
	}

	public void FooterScalePhotoPressed(){
		Debug.Log ("DesignController.FooterScalePhotoPressed");
		SetSelected(FooterButtons.SCALE_PHOTO);
		_drivewayController.MouseMode = (int)MouseModes._SCALE_PHOTO;
	}

	public void FooterTiltPlanePressed(){
		Debug.Log ("DesignController.FooterTiltPlanePressed");
		SetSelected(FooterButtons.TILT_PLANE);
		_drivewayController.MouseMode = (int)MouseModes._TILT_PLANE;
	}

	public void FooterMovePlanePressed(){
		Debug.Log ("DesignController.FooterMovePlanePressed");
		SetSelected(FooterButtons.MOVE_PLANE);
		_drivewayController.MouseMode = (int)MouseModes._MOVE_PLANE;
	}

	public void FooterRotatePlanePressed(){
		Debug.Log ("DesignController.FooterRotatePlanePressed");
		SetSelected(FooterButtons.ROTATE_PLANE);
		_drivewayController.MouseMode = (int)MouseModes._ROTATE_PLANE;
	}

	public void FooterPerspectivePressed(){
		Debug.Log ("DesignController.FooterPerspectivePressed");
		if (_drivewayController!=null) _drivewayController.ShowPlane(true);
		SetFooterState("tilt_plane");

	}

	void SetSelected(FooterButtons btn){
		if (!_selected) return;
		Transform t = transform.Find ("Footer/Buttons");
		Transform btnT = t.GetChild ((int)btn); 
		_selected.SetActive(true);
		_selected.transform.SetParent (btnT, false);
	}

	void ShowOutline(bool show){
		GameObject go = GameObject.Find("CanvasBehind3D/Image");
		if (go){
			for(int i=0; i<go.transform.childCount; i++){
				GameObject child = go.transform.GetChild(i).gameObject;
				if (child) child.SetActive(show);
			}
		}
	}

	void StoreImageTransform(){
		GameObject go = GameObject.Find ("CanvasBehind3D/Image"); 
		_imagePos = go.transform.position;
		_imageRot = go.transform.rotation;
		_imageScale = go.transform.localScale;
	}

	void Set3DActive(bool mode){
		if (_threeD) {
			for (int i=0; i<_threeD.transform.childCount; i++) {
				GameObject child = _threeD.transform.GetChild(i).gameObject;
				if (child.name != "driveway_plane")
					child.SetActive (mode);
			}
		}
	}

	public void SetFooterState(string state){
		if (state == _editState) return;

		SetDeleteBtn(false);
		if (_selected) _selected.SetActive (false);

		string leftButton = "";
		List<GameObject> btns = new List<GameObject>(); 
		int help = 0;
		bool done = false;

		Transform t = transform.Find ("Footer/Buttons");
		if (_drivewayController!=null) _drivewayController.ShowPlane(false);
		DV_UI.DrawShape.Clear();
		//_threeD.SetActive(true);
		ShowOutline(false);

		switch(state){
		case "position_photo":
			leftButton = "Cancel";
			done = true;
			StoreImageTransform ();
			btns.Add (t.GetChild ((int)FooterButtons.ROTATE_PHOTO).gameObject);
			btns.Add (t.GetChild ((int)FooterButtons.POSITION_PHOTO).gameObject);
			btns.Add (t.GetChild ((int)FooterButtons.SCALE_PHOTO).gameObject);
			btns.Add (t.GetChild ((int)FooterButtons.PERSPECTIVE).gameObject);
			if (_threeD) {
				_threeD.SetActive (true);
				EnableThreeDChildren (false);
			}
			FooterPositionPhotoPressed (false);
			break;
		case "tilt_plane":
			leftButton = "Cancel";
			done = true;
			btns.Add (t.GetChild ((int)FooterButtons.TILT_PLANE).gameObject);
			btns.Add (t.GetChild ((int)FooterButtons.MOVE_PLANE).gameObject);
			btns.Add (t.GetChild ((int)FooterButtons.ROTATE_PLANE).gameObject);
			if (_drivewayController!=null) _drivewayController.ShowPlane(true);
			ShowOutline(true);
			break;
		case "draw_paving":
			leftButton = "Cancel";
			done = true;
			DV_UI.DrawShape.enabled = true;
			_drivewayController.MouseMode = (int)MouseModes._DRAW_OUTLINE;
			btns.Add (t.GetChild ((int)FooterButtons.RESTART).gameObject);
			btns.Add (t.GetChild ((int)FooterButtons.UNDO).gameObject);
			btns.Add (t.GetChild ((int)FooterButtons.POSITION_PHOTO).gameObject);
			btns.Add (t.GetChild ((int)FooterButtons.PERSPECTIVE).gameObject);
			Set3DActive (false);
			break;
		case "paving_complete":
		case "edit":
			leftButton = "Change";
			_type = "paving";
			btns.Add (t.GetChild ((int)FooterButtons.CHANGE).gameObject);
			btns.Add (t.GetChild ((int)FooterButtons.FEATURES).gameObject);
			btns.Add (t.GetChild ((int)FooterButtons.KERBS).gameObject);
			btns.Add (t.GetChild ((int)FooterButtons.ACCESSORIES).gameObject);
			Set3DActive (true);
			break;
		case "draw_kerb":
			leftButton = "Draw";
			help = 2;
			done = true;
			btns.Add (t.GetChild ((int)FooterButtons.RESTART).gameObject);
			btns.Add (t.GetChild ((int)FooterButtons.UNDO).gameObject);
			break;
		case "paving_selected":
			//leftButton = "Delete";
			_type = "paving";
			btns.Add (t.GetChild ((int)FooterButtons.POSITION).gameObject);
			btns.Add (t.GetChild ((int)FooterButtons.ROTATE).gameObject);
			btns.Add (t.GetChild ((int)FooterButtons.CHANGE).gameObject);

			break;
		case "feature_selected":
			leftButton = "Delete";
			_type = "features";
			btns.Add (t.GetChild ((int)FooterButtons.POSITION).gameObject);
			btns.Add (t.GetChild ((int)FooterButtons.ROTATE).gameObject);
			btns.Add (t.GetChild ((int)FooterButtons.CHANGE_FEATURE).gameObject);
			break;
		case "kerb_selected":
			leftButton = "Delete";
			_type = "edging";
			btns.Add (t.GetChild ((int)FooterButtons.CHANGE_KERB).gameObject);
			break;
		case "accessory_selected":
			leftButton = "Delete";
			_type = "accessories";
			btns.Add (t.GetChild ((int)FooterButtons.POSITION).gameObject);
			btns.Add (t.GetChild ((int)FooterButtons.ROTATE).gameObject);
			btns.Add (t.GetChild ((int)FooterButtons.CHANGE_ACCESSORY).gameObject);
			break;
		default:
			Debug.Log (string.Format ("DesignController.SetFooterState {0} not supported", state));
			return;
		}

		_editState = state;

		GameObject go = transform.Find ("Footer/DeleteButton").gameObject;
		go.SetActive (leftButton=="Delete");
		go = transform.Find ("Footer/ChangeButton").gameObject;
		go.SetActive (leftButton=="Change");
		go = transform.Find ("Footer/CancelButton").gameObject;
		go.SetActive (leftButton=="Cancel");
		go = transform.Find ("Footer/PhotoButton").gameObject;
		go.SetActive (leftButton=="Photo");
		go = transform.Find ("Footer/DoneButton").gameObject;
		go.SetActive (done);
		go = transform.Find ("Footer/Help1").gameObject;
		go.SetActive (help==1);
		go = transform.Find ("Footer/Help2").gameObject;
		go.SetActive (help==2);
		go = transform.Find ("Footer/Help3").gameObject;
		go.SetActive (help==3);

		for(int i=0; i<t.childCount; i++){
			go = t.GetChild (i).gameObject;
			go.SetActive(false);
		}

		int left = 0;
		int cellwidth = 244;
		float delay = 0.0f;

		for(int i=0; i<btns.Count; i++){
			Vector3 pos = btns[i].transform.position;
			pos.x = 2048;
			btns[i].transform.position = pos;
			btns[i].SetActive (true);
			iTween.MoveTo(btns[i], iTween.Hash("x", left, "time", 0.5f, "easeType", "easeOutQuad", "loopType", "none", "delay", delay, "isLocal", true));
			delay += 0.05f;
			left += cellwidth;
		}
	}
}
