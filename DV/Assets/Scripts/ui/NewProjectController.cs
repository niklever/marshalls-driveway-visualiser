﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class NewProjectController : MonoBehaviour {
	static InputField _nameTxt;

	// Use this for initialization
	void Start () {
		GameObject go = transform.Find ("NameTxt").gameObject;
		_nameTxt = go.GetComponent<InputField> ();
	}
	
	public void Clear(){
		if (_nameTxt == null) {
			GameObject go = transform.Find ("NameTxt").gameObject;
			_nameTxt = go.GetComponent<InputField> ();
		}
		_nameTxt.text = "";
	}

	public static string Name {
		get {
			return _nameTxt.text;
		}
	}

	public void Show(bool mode){
		gameObject.SetActive (mode);
	}
}
