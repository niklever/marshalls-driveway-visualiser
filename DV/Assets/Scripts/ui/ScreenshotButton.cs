﻿using UnityEngine;
using System.Collections;

public class ScreenshotButton : MonoBehaviour {
	GameObject _deleteBtn;
	GameObject _confirmBtn;
	float _confirmTime;
	bool _confirm = false;

	// Use this for initialization
	void Start () {
		_deleteBtn = transform.Find ("DeleteButton").gameObject;
		_confirmBtn = transform.Find ("ConfirmButton").gameObject;
		_confirmBtn.SetActive (false);
	}

	void Update(){
		if (_confirm){
			float elapsedTime = Time.time - _confirmTime;
			if (elapsedTime>3){
				_confirm = false;
				_deleteBtn.SetActive (true);
				_confirmBtn.SetActive(false);
			}
		}
	}

	public void Init() {
	
	}

	public void SelectPressed(){
		Debug.Log ("ScreenshotButton.SelectPressed");
		DV_UI.CloseModalPanel();
	}

	public void DeletePressed(){
		Debug.Log ("ScreenshotButton.DeletePressed");
		_deleteBtn.SetActive (false);
		_confirmBtn.SetActive(true);
		_confirm = true;
		_confirmTime = Time.time;
	}

	public void ConfirmPressed(){
		Debug.Log ("ScreenshotButton.ConfirmPressed");
		Destroy(gameObject);
	}
}
