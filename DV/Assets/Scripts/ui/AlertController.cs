﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class AlertController : MonoBehaviour {

	void Start(){
	}

	// Use this for initialization
	public void Show (string title, string message) {
		GameObject go = transform.GetChild (0).gameObject;
		Text txt = go.GetComponent<Text>();
		txt.text = title;
		go = transform.GetChild (1).gameObject;
		txt = go.GetComponent<Text>();
		txt.text = message;
		gameObject.SetActive (true);
	}
	
	public void Hide(){
		gameObject.SetActive (false);
	}
}
