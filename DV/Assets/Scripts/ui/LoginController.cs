﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class LoginController : MonoBehaviour {
	InputField _email;
	InputField _password;
	DV_UI _dv_ui;

	// Use this for initialization
	void Start () {
		GameObject go = GameObject.Find ("Canvas/LoginPanel/EmailTxt");
		if (go) _email = go.GetComponent<InputField>();
		go = GameObject.Find ("Canvas/LoginPanel/PasswordTxt");
		if (go) _password = go.GetComponent<InputField>();
		string str = PlayerPrefs.GetString("email", "");
		if (str!="") _email.text = str;
		str = PlayerPrefs.GetString("password", "");
		if (str!="") _password.text = str;
		go = GameObject.Find ("Canvas");
		_dv_ui = go.GetComponent<DV_UI>();
	}

	public void LoginPressed(){
		DVWebService.LoginHandler(_email.text, _password.text);
		Analytics.ga ("User account", "Login");
	}

	public void ForgotPressed(){
		DV_UI.Animate ("forgotIn");
	}

	public void RegisterPressed(){
		DV_UI.Animate ("registerIn");
	}
}
