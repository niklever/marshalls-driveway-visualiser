﻿using UnityEngine;
using UnityEngine.UI;
using System;
using System.Collections;
using System.Collections.Generic;
using SimpleJSON;

public class ProjectsController : MonoBehaviour {
	public GameObject _thumbnail;

	DV_UI _dv_ui;
	GameObject _mainButtons;
	GameObject _editButtons;
	GameObject _confirmButton;
	Text _title;
	Text _confirmTxt;
	Image _editBg;
	Image _trashImg;
	Image _duplicateImg;
	Button _trashBtn;
	Button _duplicateBtn;

	// Use this for initialization
	void Start () {
		GameObject go = GameObject.Find ("Canvas");
		_dv_ui = go.GetComponent<DV_UI>();
		ClearThumbnails();
		_mainButtons = GameObject.Find ("Canvas/ProjectsPanel/Header/MainButtons");
		_editButtons = GameObject.Find ("Canvas/ProjectsPanel/Header/EditButtons");
		_confirmButton = GameObject.Find ("Canvas/ProjectsPanel/Header/EditButtons/ConfirmButton");
		go = GameObject.Find ("Canvas/ProjectsPanel/Header/EditButtons/ConfirmButton/Text");
		_confirmTxt = go.GetComponent<Text>();
		go = GameObject.Find ("Canvas/ProjectsPanel/Header/EditButtons/DeleteButton");
		_trashImg = go.GetComponent<Image>();
		_trashBtn = go.GetComponent<Button>();
		go = GameObject.Find ("Canvas/ProjectsPanel/Header/EditButtons/DuplicateButton");
		_duplicateImg = go.GetComponent<Image>();
		_duplicateBtn = go.GetComponent<Button>();
		_editButtons.SetActive(false);
		_confirmButton.SetActive (false);
		go = GameObject.Find ("Canvas/ProjectsPanel/EditBg");
		_editBg = go.GetComponent<Image>();
		go = GameObject.Find ("Canvas/ProjectsPanel/Header/Text");
		_title = go.GetComponent<Text>();
	}

	void ClearThumbnails(){
		Transform t = transform.Find ("Projects/Scroller");
		if (t){
			for(int i=t.childCount-1; i>=0; i--) Destroy(t.GetChild (i).gameObject);
		}
		GameObject go = GameObject.Find ("ProjectThumbnail(Clone)");
		if (go != null){
			Destroy (go);
		}
	}

	public void UpdatePanel(JSONNode data, bool show=true){
		ClearThumbnails();
		Debug.Log ("ProjectsController.UpdatePanel");
		Transform t = transform.Find ("Projects/Scroller");
		JSONArray projects = data.AsArray;
		//Debug.Log (projects);
		GameObject go = null;
		for(int i=0; i<projects.Count; i++){
			JSONNode project = projects[i];
			if (project!=null && project!="null"){
				try{
					go = (GameObject)Instantiate(_thumbnail);
					ThumbnailController controller = go.GetComponent<ThumbnailController>();
					controller.Set(project);
					go.transform.SetParent(t, false);
				}catch(ArgumentNullException e){
					Debug.Log ("ProjectsController.UpdatePanel " + e);
				}
			}
		}
		int rows = (int)Math.Ceiling ((double)(data.Count/4.0));
		ResizePanelAfterLoad(rows, 444);
		if (show) DV_UI.Animate ("projectsIn");
	}

	void ResizePanelAfterLoad(int count, int cellSize){
		GameObject go = GameObject.Find ("Canvas/ProjectsPanel/Projects/Scroller");
		RectTransform rt = go.GetComponent<RectTransform>();
		Vector2 size = rt.sizeDelta;
		size.y = Math.Max(1410, count * cellSize + 85);
		rt.sizeDelta = size;
		Vector2 pos = rt.anchoredPosition;
		pos.y = 0;
		rt.anchoredPosition = pos;
	}

	public void NewProjectPressed(){
		Debug.Log ("ProjectsController.NewProjectPressed");
		DV_UI.ShowModalPanel("NewProject");
	}

	public void ActionPressed(){
		Debug.Log ("ProjectsController.ActionPressed");
		SetEditState(true);
	}

	void SetEditState(bool mode){
		if (_editBg) _editBg.enabled = mode;
		Transform t = transform.Find ("Projects/Scroller");
		for(int i=0; i<t.childCount; i++){
			ThumbnailController controller = t.GetChild (i).gameObject.GetComponent<ThumbnailController>();
			controller.SetEditState(mode);
		}
		_mainButtons.SetActive (!mode);
		_editButtons.SetActive (mode);
		if (mode){
			_title.text = "SELECT A PROJECT";
			_trashImg.color = new Color(1,1,1,0.5f);
			_duplicateImg.color = new Color(1,1,1,0.5f);
			UpdateEditHeader();
		}else{
			_title.text = "DRIVEWAY PROJECTS";
		}
	}

	public void UpdateEditHeader(){
		Color col = (SelectedCount==0) ? new Color(1,1,1,0.5f) : new Color(1,1,1,1);
		_trashImg.color = col;
		_duplicateImg.color = col;
		_trashBtn.interactable = (SelectedCount>0);
		_duplicateBtn.interactable = (SelectedCount>0);
	}

	public void InfoPressed(){
		Debug.Log ("ProjectsController.InfoPressed");
		//DV_UI.ShowModalPanel ("Help");
	#if UNITY_EDITOR
		Application.OpenURL (string.Format ("{0}dv/help/index.php", DVWebService.ResourcePath));
	#else
		#if UNITY_WEBPLAYER
			Application.ExternalCall("showHelp", true);
		#else
		Application.OpenURL (string.Format ("{0}dv/help/index.php", DVWebService.ResourcePath));
		#endif
	#endif
	}

	public void SettingsPressed(){
		Debug.Log ("ProjectsController.SettingsPressed");
		DV_UI.ShowModalPanel ("Settings");
	}

	public int SelectedCount{
		get{
			int count = 0;
			Transform t = transform.Find ("Projects/Scroller");
			for(int i=0; i<t.childCount; i++){
				ThumbnailController controller = t.GetChild (i).gameObject.GetComponent<ThumbnailController>();
				if (controller.Selected) count++;
			}
			return count;
		}
	}
	
	public void DeletePressed(){
		Debug.Log ("ProjectsController.DeletePressed");
		if (_confirmButton.activeSelf){
			_confirmButton.SetActive (false);
		}else{
			_confirmTxt.text = string.Format ("Delete {0} projects", SelectedCount);
			_confirmButton.SetActive(true);
		}
	}

	public void DuplicatePressed(){
		Debug.Log ("ProjectsController.DuplicatePressed");
		Transform t = transform.Find ("Projects/Scroller");
		List<string> guids = new List<string>();
		
		for(int i=0; i<t.childCount; i++){
			ThumbnailController controller = t.GetChild (i).gameObject.GetComponent<ThumbnailController>();
			if (controller.Selected) guids.Add (controller.Guid );
		}

		DVWebService.CloneGuids = guids;//This will start the cloning process one project at a time.
		SetEditState(false);
	}

	public void DonePressed(){
		Debug.Log ("ProjectsController.DonePressed");
		SetEditState(false);
	}

	public void ConfirmPressed(){
		Debug.Log ("ProjectsController.ConfirmPressed");
		Transform t = transform.Find ("Projects/Scroller");
		List<GameObject> remove = new List<GameObject>();
		List<string> deletedGuids = new List<string>();

		for(int i=0; i<t.childCount; i++){
			ThumbnailController controller = t.GetChild (i).gameObject.GetComponent<ThumbnailController>();
			if (controller.Selected){
				remove.Add(controller.gameObject);
				deletedGuids.Add (controller.Guid );
			}
		}

		DVWebService.DeleteProjects(deletedGuids);

		while(remove.Count>0){
			GameObject go = remove[0];
			remove.Remove(go);
			Destroy(go);
		}

		_confirmButton.SetActive (false);
		SetEditState(false);
	}

	public void SettingsClosePressed(){
		Debug.Log ("ProjectsController.SettingsClosePressed");
		DV_UI.CloseModalPanel ();
	}

	public void HelpClosePressed(){
		Debug.Log ("ProjectsController.HelpClosePressed");
		DV_UI.CloseModalPanel ();
	}

	public string GetBase64FromGuid(string guid){
		string result = "";

		Transform t = transform.Find ("Projects/Scroller");
		for(int i=0; i<t.childCount; i++){
			ThumbnailController controller = t.GetChild (i).gameObject.GetComponent<ThumbnailController>();
			if (controller.Guid == guid){
				result = controller.Base64;
				break;
			}
		}
		
		return result;
	}
}
