﻿using UnityEngine;
using UnityEngine.UI;
using Vectrosity;
using System.Collections;
using System.Collections.Generic;

public class DrawShape : MonoBehaviour {
	public Texture2D _dotTexture;
	public Texture2D _lineTexture;

	GameObject _lineGO;
	VectorLine _line;
	VectorLine _dots;
	bool _drawing = false;
	bool _closed = false;
	bool _close = true;
	int _dragIndex = -1;
	float _clickTime;
	float [] _mouseLegal;

	public bool Close {
		get {
			return _close;
		}
		set {
			_close = value;
		}
	}
	
	// Use this for initialization
	void Start () {
		_lineGO = transform.Find ("Line").gameObject;
		VectorObject2D line = _lineGO.GetComponent<VectorObject2D>();
		_line = line.vectorLine;
		_lineGO.SetActive(false);
		_line.points2.Clear();
		_clickTime = Time.time;
		_dots = new VectorLine("Dots", new List<Vector2>(), _dotTexture, 10.0f, LineType.Points);
		_line = new VectorLine("Line", new List<Vector2>(), _lineTexture, 3.0f, LineType.Continuous);
		_line.textureScale = 3;
		enabled = false;
		GameObject go = GameObject.Find ("Canvas");
		RectTransform rt = go.GetComponent<RectTransform>();
		_mouseLegal = new float[2];
		#if UNITY_EDITOR
			_mouseLegal[0] = 100 * rt.localScale.y;
			_mouseLegal[1] = 700 * rt.localScale.y;
		#else
			#if UNITY_WEBPLAYER
				//float scale = 960.0f/1024.0f;
				_mouseLegal[0] = 100;
				_mouseLegal[1] = 700;
			#else
				_mouseLegal[0] = 100 * rt.localScale.y;
				_mouseLegal[1] = 700 * rt.localScale.y;
			#endif
		#endif
		Vector2 s = new Vector2(rt.sizeDelta.x * rt.localScale.x, rt.sizeDelta.y * rt.localScale.y);
		Debug.Log(string.Format("Mouse legal {0},{1} scale:{2} screen:{3},{4}", _mouseLegal[0], _mouseLegal[1], rt.localScale.y, s.x, s.y));
		DV_UI.DrawShape = this;
	}

	public void Clear(){
		_dots.points2.Clear();
		_line.points2.Clear();
		_dots.Draw ();
		_line.Draw ();
		enabled = false;
	}

	public void Restart(){
		Clear();
		_dragIndex = -1;
		_drawing = false;
		enabled = true;
	}

	public void Undo(){
		_line.points2.RemoveAt(_line.points2.Count-1);
		_line.Draw();
		_dots.points2.RemoveAt(_dots.points2.Count-1);
		_dots.Draw();
	}

	bool MouseLegal(){
		//Debug.Log(string.Format("mousePosition:{0},{1}", Input.mousePosition.x, Input.mousePosition.y));
		return Input.mousePosition.y>_mouseLegal[0] && Input.mousePosition.y<_mouseLegal[1];
	}

	// Update is called once per frame
	void Update () {
		bool modified = false;

		if (Input.GetMouseButtonDown(0) && MouseLegal()){
			Vector2 mousePosition = new Vector2(Input.mousePosition.x, Input.mousePosition.y);
			float elapsedTime = Time.time - _clickTime;
			bool doubleClick = (elapsedTime<0.5f);
			_dragIndex = -1;
			int insertIndex = -1;
			int index;
			
			if (!_drawing){
				if (_line.points2.Count>0){
					for(int i=0; i<_line.points2.Count; i++){
						if (Vector2.Distance (_line.points2[i], mousePosition)<15){
							_dragIndex = i;
							break;
						}
					}
					if (_dragIndex==-1){
						_line.Selected (mousePosition, 15, out index);
						insertIndex = index;
					}
					
					if (doubleClick && _dragIndex!=-1 && insertIndex==-1){
						if (_dragIndex<_line.points2.Count) _line.points2.RemoveAt(_dragIndex);
						_line.Draw ();
						modified = true;
						_dragIndex = -1;
					}
				}
				
				if (_dragIndex==-1 && insertIndex==-1 && !doubleClick){
					_line.points2.Clear();
					_drawing = true;
					_closed = false;
					_line.points2.Add (mousePosition);
					modified = true;
				}
				//_lineGO.SetActive (true);
			}
			
			if (doubleClick && _line.points2.Count>0 && _closed){
				if (_dragIndex==-1){
					_line.Selected (mousePosition, 15, out index);
					if (index!=-1){
						if (index<_line.points2.Count){
							_line.points2.Insert(index+1, mousePosition);
						}else{
							_line.points2.Add (mousePosition);
						}
						_line.Draw ();
						modified = true;
						insertIndex = index;
					}
				}
			}
			
			//Debug.Log ("TestDrawing.OnMouseDown " + Input.mousePosition);
			if (_dragIndex==-1 && insertIndex==-1){
				if (doubleClick){
					//Close shape
					_drawing = false;
					_closed = true;
					if (_line.points2.Count>0 && _close){
						_line.points2[_line.points2.Count-1] = _line.points2[0];
						_line.Draw ();
						modified = true;
					}
					_close = true;
				}else{
					//Add new point
					_line.points2.Add (mousePosition);
					modified = true;
				}
			}
			_clickTime = Time.time;
		}
		
		if (!Input.GetMouseButton(0)) _dragIndex = -1;
		
		if (_drawing && _line.points2.Count>0){
			if (MouseLegal()){
				Vector2 point = _line.points2[_line.points2.Count-1];
				point.x = Input.mousePosition.x;
				point.y = Input.mousePosition.y;
				_line.points2[_line.points2.Count-1] = point;
				_line.Draw ();
				modified = true;
			}
		}else if (_dragIndex!=-1){
			Vector2 point = _line.points2[_dragIndex];
			point.x = Input.mousePosition.x;
			point.y = Input.mousePosition.y;
			if (_dragIndex<_line.points2.Count) _line.points2[_dragIndex] = point;
			if (_dragIndex==0) _line.points2[_line.points2.Count-1] = point;
			_line.Draw ();
			modified = true;
		}

		if (modified){
			_dots.points2.Clear ();
			for(int i=0; i<_line.points2.Count; i++){
				if (i==_line.points2.Count-1 && _closed) break;
				_dots.points2.Add (_line.points2[i]);
			}
			_dots.Draw ();
		}
	}

	public List<Vector2> SourceVertices2d{
		get{
			if (_line==null) return null;
			return _line.points2;
		}
	}
}