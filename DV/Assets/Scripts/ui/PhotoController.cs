﻿using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif
using UnityEngine.UI;
using System.Collections;
using Ucss;

public class PhotoController : MonoBehaviour {
	string _guid;
	string _url;
	GameObject _newPanel;
	GameObject _drawPanel;
	DesignController _designController;
	DesignController DesignController{
		get{
			if (_designController==null){
				_designController = DV_UI.DesignController;
			}
			return _designController;
		}
	}
	DrivewayController _drivewayController;
	DrivewayController DrivewayController{
		get{
			if (_drivewayController==null){
				GameObject go = GameObject.Find ("3D");
				_drivewayController = go.GetComponent<DrivewayController>();
			}
			return _drivewayController;
		}
	}
	// Use this for initialization
	void Start () {
		_newPanel = transform.GetChild (0).gameObject;
		_drawPanel = transform.GetChild (1).gameObject;
		_drawPanel.SetActive(false);

#if UNITY_WEBPLAYER
		GameObject go = _newPanel.transform.GetChild(0).gameObject;
		Button btn = go.GetComponent<Button>();
		btn.interactable = false;
		Image img = go.GetComponent<Image>();
		img.color = new Color(1,1,1,0.5f);
#endif
	}
	
	public void BackPressed(){
		DV_UI.CloseModalPanel();
		DV_UI.DrawShape.enabled = false;
	#if UNITY_EDITOR
	#else
		#if UNITY_WEBPLAYER
			Application.ExternalEval("selectHouseImage(false)");
		#endif
	#endif
	}

	public void TakePressed(){
		Debug.Log ("PhotoController.TakePressed");
	}

	public void ChoosePressed(){
		Debug.Log ("PhotoController.ChoosePressed");


#if UNITY_EDITOR
		string path = EditorUtility.OpenFilePanel("Select an image", "", "jpg");
		if (path.Length != 0) {
			Debug.Log (string.Format ("PhotoController.ChoosePressed path:{0}", path));
			Texture2D texture = new Texture2D(16,16); 
			WWW www = new WWW("file:///" + path);
			www.LoadImageIntoTexture(texture);
			byte[] bytes = texture.EncodeToPNG();
			DrivewayController.Project.HouseSize = texture.width;
			DrivewayController.Project.HouseAspect = texture.width/texture.height;
			DrivewayController.Project.HouseDefault = false;
			DrivewayController.Project.Thumbnail = System.Convert.ToBase64String (bytes);
			DV_UI.ProjectImage.sprite = Sprite.Create (texture, new Rect(0,0,texture.width,texture.height), Vector2.zero);
			DVWebService.UploadImage(bytes, true);
		}
#else
	#if UNITY_WEBPLAYER
		Application.ExternalEval(string.Format("selectHouseImage(true, \"{0}\")", DVWebService.Token));
	#elif UNITY_IOS

	#elif UNITY_ANDROID

	#endif
#endif
	}

	public void DrawPressed(){
		if (DesignController!=null) _designController.SetFooterState("position_photo");
		DV_UI.CloseModalPanel();
		DV_UI.ShowDesignPanel(true);
	#if UNITY_EDITOR
	#else
		#if UNITY_WEBPLAYER
		Application.ExternalEval("selectHouseImage(false)");
		#endif
	#endif
	}

	public void Show(bool draw){
		gameObject.SetActive(true);
		if (draw){
			_newPanel.SetActive(false);
			_drawPanel.SetActive(true);
			GameObject go = transform.Find ("DrawPanel/ImagePanel/Image").gameObject;
			Image image = go.GetComponent<Image>();
			image.sprite = DV_UI.ProjectImage.sprite;
		}else{
			_newPanel.SetActive(true);
			_drawPanel.SetActive(false);
		}
	}

	public void WebImageCallback(string url, string guid){
		Debug.Log(string.Format("PhotoController.WebImageCallback url:{0} guid:{1}", url, guid));
		UCSS.HTTP.GetTexture ( url, this.WebLoadCallback, this.WebLoadError);
		_guid = guid;
		_url = url;
	}

	void WebLoadError(string error, string id){
		Debug.Log("PhotoController.WebLoad download error:" + error);
		DV_UI.ShowError ( "Problem accessing image");
		_guid = _url = "";
	}

	void WebLoadCallback(Texture2D texture, string id){
		Debug.Log("PhotoController.WebLoad texture:" + id);
		byte[] bytes = texture.EncodeToPNG();
		DrivewayController.Project.HouseSize = texture.width;
		DrivewayController.Project.HouseAspect = texture.width/texture.height;
		DrivewayController.Project.HouseDefault = false;
		DrivewayController.Project.Thumbnail = System.Convert.ToBase64String (bytes);
		DrivewayController.Project.Photos.Add(_guid);
		DrivewayController.Project.HousePattern = _url;
		DV_UI.ProjectImage.sprite = Sprite.Create (texture, new Rect(0,0,texture.width,texture.height), Vector2.zero);
		Show(true);
		DVWebService.SaveNewProject(DrivewayController.Project);
		if (DrivewayController) _drivewayController.Init(DrivewayController.Project, true);
		DV_UI.ShowDesignPanel (true);
	}
}
