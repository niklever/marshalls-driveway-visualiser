﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class RegisterController : MonoBehaviour {
	DV_UI _dv_ui;

	// Use this for initialization
	void Start () {
		GameObject go = GameObject.Find ("Canvas");
		_dv_ui = go.GetComponent<DV_UI>();
	}
	
	public void BackPressed(){
		DV_UI.Animate ("loginIn");
	}

	public void CreatePressed(){
		GameObject go = transform.Find ("FirstNameTxt").gameObject;
		InputField txt = go.GetComponent<InputField>();
		string firstname = txt.text;
		go = transform.Find ("LastTxt").gameObject;
		txt = go.GetComponent<InputField>();
		string lastname = txt.text;
		go = transform.Find ("EmailTxt").gameObject;
		txt = go.GetComponent<InputField>();
		string email = txt.text;
		go = transform.Find ("PasswordTxt").gameObject;
		txt = go.GetComponent<InputField>();
		string password = txt.text;
		DVWebService.CreateAccount(firstname, lastname, email, password);
	}
}
