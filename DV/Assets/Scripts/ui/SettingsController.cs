﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections;

public class SettingsController : MonoBehaviour {
	EventSystem _system;
	InputField _firstNameTxt;
	InputField _lastNameTxt;
	InputField _emailTxt;
	InputField _passwordTxt;

	DV_UI _dv_ui;

	// Use this for initialization
	void Start () {
		_system = EventSystem.current;
	}

	public void Init(){
		GameObject go = transform.Find ("FirstNameTxt").gameObject;
		_firstNameTxt = go.GetComponent<InputField>();
		go = transform.Find ("LastTxt").gameObject;
		_lastNameTxt = go.GetComponent<InputField>();
		go = transform.Find ("EmailTxt").gameObject;
		_emailTxt = go.GetComponent<InputField>();
		go = transform.Find ("PasswordTxt").gameObject;
		_passwordTxt = go.GetComponent<InputField>();
		go = GameObject.Find("Canvas");
		_dv_ui = go.GetComponent<DV_UI>();
	}

	// Update is called once per frame
	void Update () {
		if (Input.GetKeyDown(KeyCode.Tab)){
			Selectable next = _system.currentSelectedGameObject.GetComponent<Selectable>().FindSelectableOnDown();
			
			if (next != null){
				InputField inputfield = next.GetComponent<InputField>();
				if (inputfield != null)
					inputfield.OnPointerClick(new PointerEventData(_system));  //if it's an input field, also set the text caret
				
				_system.SetSelectedGameObject(next.gameObject, new BaseEventData(_system));
			}else{
				next = Selectable.allSelectables[0];
				_system.SetSelectedGameObject(next.gameObject, new BaseEventData(_system));
			}
		}
	}

	public void SavePressed(){
		Debug.Log ("SettingsController.SavePressed");
		if (DVWebService.UpdateAccount(_firstNameTxt.text, _lastNameTxt.text, _emailTxt.text, _passwordTxt.text)){
			Analytics.ga ("User account", "Change settings");
			DV_UI.CloseModalPanel ();
		}
	}
	
	public void LogoutPressed(){
		Debug.Log ("SettingsController.LogoutPressed");
		Analytics.ga ("User account", "Logout");
		DV_UI.CloseModalPanel ();
		_dv_ui.DrivewayController.Clear();
		DV_UI.ShowDesignPanel (false);
		DV_UI.ShowProjectsPanel (false);
		DV_UI.Animate ("loginIn");
	}

	public void UpdatePanel(string userName, string password, string firstName, string lastName){
		_firstNameTxt.text = firstName;
		_lastNameTxt.text = lastName;
		_emailTxt.text = userName;
		_passwordTxt.text = password;
	}

	public void Show(){
		DVWebService.GetAccount ();
		gameObject.SetActive(true);
	}
}
