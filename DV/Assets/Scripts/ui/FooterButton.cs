﻿using UnityEngine;
using System.Collections;

public class FooterButton : MonoBehaviour {
	static float _offScreenX = 1700;
	static float _duration = 1.0f;

	RectTransform _rt;
	bool _animating = false;
	float _startTime;
	float _startX;
	float _targetX;

	public bool Active{
		set{
			Vector2 pos = _rt.anchoredPosition;
			if (!value){
				pos.x = _offScreenX;
				_rt.anchoredPosition = pos;
			}
		}
	}

	public float TargetX{
		set{
			_startTime = Time.time;
			_targetX = value;
			Active = false;//This will position it off screen
			_animating = true;
		}
	}

	// Use this for initialization
	void Start () {
		_rt = GetComponent<RectTransform>();
	}
	
	// Update is called once per frame
	void Update () {
		if (_animating){
			float elapsedTime = Time.time - _startTime;
			float x = (_offScreenX - _targetX) * ((_duration-elapsedTime)/_duration) + _targetX;
			if (elapsedTime>_duration){
				x = _targetX;
				_animating = false;
			}
			Vector2 pos = _rt.anchoredPosition;
			pos.x = x;
			_rt.anchoredPosition = pos;
		}
	}
}
