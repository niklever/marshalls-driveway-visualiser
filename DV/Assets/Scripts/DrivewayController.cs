﻿using UnityEngine;
using System;
using System.Xml;
using System.Text;
using System.Collections;
using System.Collections.Generic;
using SimpleJSON;
using Random = UnityEngine.Random;

enum MouseModes{_NONE, _MOVE, _ROTATE, _MOVE_PATTERN, _ROTATE_PATTERN, _SCALE_HOUSE, _FLIP, _ADD_DOLLY, _ADD_PILLAR, _CAMERA_TILT, 
	_CAMERA_HEIGHT, _ADD_FLAG, _ADD_ROCK, _ROTATE_PHOTO, _MOVE_PHOTO, _SCALE_PHOTO, _TILT_PLANE, _MOVE_PLANE, _ROTATE_PLANE, _DRAW_OUTLINE, _ADD_FEATURE, _ADD_ACCESSORY};

public class DrivewayController : MonoBehaviour {
	public GameObject _square_feature;
	public GameObject _circle_feature;
	public GameObject _octant_feature;
	public GameObject _bonding_feature;
	public GameObject _selectBox;

	public static JSONNode ProductsJSON;
	public static float ScaleFactor = 2.0f;

	static Vector3 _up = Vector3.zero;
	public static Vector3 Up{
		get{
			return _up;
		}
	}

	static Project _project;
	public static Project Project{
		get{
			return _project;
		}
	}
	static float _saveTime;
	static bool _active = false;
	static bool _modified = false;
	public static bool Modified { 
		get{
			return _modified;
		}
		set{
			_modified = value;
			if (!value) _modifiedTime = Time.time;
		}
	}

	static DV_UI _dv_ui;
	static List<WWWAsset> _assets;
	static private Dictionary<string, AssetBundle> _assetBundles = new Dictionary<string, AssetBundle>();
	public static List<WWWAsset> Assets {
		get {
			return _assets;
		}
		set {
			_assets = value;
		}
	}
	public static Dictionary<string, AssetBundle> AssetBundles {
		get {
			return _assetBundles;
		}
		set {
			_assetBundles = value;
		}
	}

	static DesignController _designController;
	static DesignController DesignController {
		get {
			if (_designController == null) {
				GameObject go = GameObject.Find ("Canvas/DesignPanel");
				if (go)
					_designController = go.GetComponent<DesignController> ();
			}
			return _designController;
		}
	}

	List<Feature> _features;
	List<Edging> _edging;

	bool _loading;
	float [] _mouseLegal = new float[2];
	GameObject _3d;
	GameObject _plane;
	public GameObject Plane{
		get{
			return _plane;
		}
	}
	Vector3 _planePos;
	Quaternion _planeRot;
	GameObject _drawShape;
	Driveway _driveway;
	public Driveway Driveway{
		set{
			_driveway = value;
		}
		get{
			return _driveway;
		}
	}
	bool _mouseDown = false;
	Transform _selectedT = null;
	public Transform SelectedT{
		get{
			return _selectedT;
		}
		set{
			_selectedT = value;
		}
	}
	float _clickTime = 0.0f;
	Vector3 _initDragMousePos;
	Vector3 _offset2;
	Vector3 _offset1;
	Vector3 _screenPoint;
	Vector3 _rotationCentre;
	Vector3 _prevMousePosition;
	int _mouseMode;
	public int MouseMode{
		get{
			return _mouseMode;
		}
		set{
			_mouseMode = value;
		}
	}
	public bool DrivewaySelected{
		get{
			if (_selectedT!=null && IsDriveway(_selectedT)) return true;
			return false;
		}
	}
	Product _storedProduct;
	Product _selectedProduct;
	public Product SelectedProduct{
		get{
			return _selectedProduct;
		}
		set{
			_selectedProduct = value;
		}
	}
	Vector2 _camMousePos;
	static float _modifiedTime;

	// Use this for initialization
	void Start () {
		DVWebService.LoadProductsConfig();
		_mouseLegal[0] = 100;
		_mouseLegal[1] = 700;
		if (_dv_ui==null){
			GameObject go = GameObject.Find ("Canvas");
			_dv_ui = go.GetComponent<DV_UI>();
			RectTransform rt = go.GetComponent<RectTransform>();
			//float scale = rt.sizeDelta.x/1024;
			_mouseLegal[0] = 100 * rt.localScale.x;
			_mouseLegal[1] = 700 * rt.localScale.x;
			//AddDebugSphere(new Vector3(0, _mouseLegal[0]));
			//AddDebugSphere(new Vector3(0, _mouseLegal[1]));
		}
		_3d = GameObject.Find ("3D");
		_3d.transform.position = new Vector3(-1000000, 0, 0);
		DVWebService.DrivewayController = this;
	}

	public void UpdateDriveway(List<Vector2>vertices){
		if (Project.Driveway==null){
			Project.Driveway = new Product();
		}else if (_driveway!=null && _driveway.DrivewayGO!=null){
			Destroy(_driveway.DrivewayGO);
		}

		bool removed = false;
		do{
			bool first = true;
			removed = false;
			foreach(Product p in Project.Products){
				if (p.Type=="driveway"){ 
					if (first){
						first = false;
						continue;
					}else{
						Project.Products.Remove(p);
						removed=true;
						break;
					}
				}
			}
		}while(removed);

		Product product = Project.Driveway;
		product.SourceVertices2d = new List<Vector2>();

		/*Transform t = GameObject.Find("CanvasBehind3D/Image").transform;

		List<Vector3> pts = new List<Vector3>();
		pts.Add(t.TransformPoint(new Vector3(-1590/2,-1590/2,0))); 
		pts.Add(t.TransformPoint(new Vector3( 1590/2,-1590/2,0))); 
		pts.Add(t.TransformPoint(new Vector3( 1590/2,0,0))); 
		pts.Add(t.TransformPoint(new Vector3(-1590/2,0,0))); 
		//Vector2 offset = new Vector2(pt1.x, pt1.y);
		Vector3 pt;
		for(int i=0; i<pts.Count; i++){
			AddDebugSphere(pts[i], 20, "green");
			pt = t.InverseTransformPoint(pts[i]);
			pt.z = 0;
			pt.y = 1590 - 1590/2 - pt.y;
			pt.x += 1590/2;
			product.SourceVertices2d.Add(pt);
		}
		pt = t.InverseTransformPoint(pts[0]);
		pt.z = 0;
		pt.y = 1590 - 1590/2 - pt.y;
		pt.x += 1590/2;
		product.SourceVertices2d.Add(pt);*/


		RectTransform rt = (RectTransform)GameObject.Find("VectorCanvas").transform;
		Transform t = GameObject.Find("CanvasBehind3D/Image").transform;
		Vector2 imagesize = new Vector2(2062.0f, 1540.0f);
		float scale = imagesize.x/rt.sizeDelta.x;

		for(int i=0; i<vertices.Count; i++){
			Vector3 pt = new Vector3(vertices[i].x,vertices[i].y);
			pt *= scale;
			pt.x -= imagesize.x/2;
			pt.y -= imagesize.y/2;
			pt.z = t.position.z;
			//AddDebugSphere(pt, 20, "green");
			pt = t.InverseTransformPoint(pt);
			pt.z = 0;
			pt.y = 1590 - 1590/2 - pt.y;
			pt.x += 1590/2;
			product.SourceVertices2d.Add(pt);
		}
		//if (_dv_ui!=null) _dv_ui.DrawOutline(product.SourceVertices2d);
		Modified = true;

		if (product.Pattern!=null && product.Pattern!=""){
			_driveway = new Driveway(Project, product);
		}else{
			_driveway = new Driveway(Project, product, false);
			_selectedProduct = product;
			DV_UI.ShowModalPanel("Products", "driveway", true);
		}
	}

	public static GameObject InstantiateGameObject(AssetBundle asset){
		Debug.Log ("DrivewayController.InstantiateGameObject asset:" + asset);
		GameObject go = (GameObject)Instantiate(asset.mainAsset);
		if (go.transform.childCount>0){
			for(int i=0; i<go.transform.childCount; i++){
				Transform t = go.transform.GetChild (i);
				GameObject go1 = t.gameObject;
				if (go1!=null && go1.GetComponent<Renderer>()!=null){
					go1.AddComponent<BoxCollider>();
				}
			}
		}else{
			go.AddComponent<BoxCollider>();
		}
		
		Modified = true;
		
		return go;
		
	}
	
	public static GameObject InstantiateGameObject(WWW www){
		GameObject go = (GameObject)Instantiate(www.assetBundle.mainAsset);
		if (go.transform.childCount>0){
			for(int i=0; i<go.transform.childCount; i++){
				Transform t = go.transform.GetChild (i);
				GameObject go1 = t.gameObject;
				if (go1!=null && go1.GetComponent<Renderer>()!=null){
					go1.AddComponent<BoxCollider>();
				}
			}
		}else{
			go.AddComponent<BoxCollider>();
		}
		
		Modified = true;
		
		return go;
		
	}

	Vector3 MouseToWorld(bool yZero){
		Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
		// create a plane at 0,0,0 whose normal points to +Y:
		Plane hPlane = new Plane(Vector3.up, Vector3.zero);
		// Plane.Raycast stores the distance from ray.origin to the hit point in this variable:
		float distance = 0; 
		// if the ray hits the plane...
		Vector3 pos = new Vector3(0,0,0);
		
		if (hPlane.Raycast(ray, out distance)){
			// get the hit point:
			pos = ray.GetPoint(distance);
			if (yZero) pos.y = 0;
		}
		
		return pos;
	}

	public void SelectProduct(Product product, Transform t){
		ShowSelection(t);//This sets both _selectedT and _selectedProduct and clears any previous selection
		if (_dv_ui!=null){
			if (_selectedProduct!=null) _dv_ui.ObjectSelected(true, product.Type);
		}
		_mouseMode = (int)MouseModes._MOVE;
	}
	
	public Product GetSelectedProduct(Transform t){
		bool found = false;
		string name = "";
		Product product = null;
		//Find the selected object in the lists
		if (IsDriveway(t)){
			found = true;
			name = _driveway.DrivewayGO.name;
			product = _driveway.Product;
		}
		if (!found && _features!=null && _features.Count>0){
			foreach(Feature feature in _features){
				if (feature!=null && feature.FeatureGO == t.gameObject){
					found = true;
					name = feature.FeatureGO.name;
					product = feature.Product;
					break;
				}
			}
		}
		if (!found && _edging!=null && _edging.Count>0){
			foreach(Edging edging in _edging){
				if (edging!=null && edging.EdgingGO == t.gameObject){
					found = true;
					name = edging.EdgingGO.name;
					product = edging.Product;
					break;
				}
			}
		}
		if (!found && _assets!=null && _assets.Count>0){
			foreach(WWWAsset asset in _assets){
				if (asset!=null && asset.AssetGO == t.gameObject){
					found = true;
					name = asset.AssetGO.name;
					product = asset.Product;
					break;
				}
			}
		}
		
		Debug.Log ("DrivewayController.GetSelectedProduct found:" + found + " name:" + name);
		return product;
	}

	Bounds GetBounds(Transform t){
		Bounds bounds = new Bounds();
		MeshFilter mf;
		//Mesh mesh;
		
		if (t.childCount==0){
			mf = t.gameObject.GetComponent<MeshFilter>();
			if (mf!=null){
				bounds = mf.mesh.bounds;
				bounds.center = t.localPosition;
				//Debug.Log ("DrivewayController.GetBounds name:" + t.gameObject.name + " bounds:" + bounds);
			}
		}else{
			for(int i=0; i<t.childCount; i++){
				Bounds b = GetBounds(t.GetChild (i));
				bounds.Encapsulate(b.min);
				bounds.Encapsulate(b.max);
			}
		}
		
		return bounds;
	}

	bool UseSelectionBox(Transform t){
		string name = "";
		if (t.gameObject.GetComponent<Renderer>()!=null){
			name = t.gameObject.name;
		}else{
			for(int i=0; i<t.childCount; i++){
				Transform tt = t.GetChild (i);
				if (tt.GetComponent<Renderer>()!=null){
					name = tt.gameObject.name;
					break;
				}
			}
		}
		string [] items = {"vegetation", "tree", "greenhouse", "greenshourse", "summerhouse"};
		bool found = false;
		
		for(int i=0; i< items.Length; i++){
			if (name.Contains (items[i])){
				found = true;
				break;
			}
		}
		
		return found;
	}

	public void ShowSelection(Transform t){
		if (t == _selectedT) return;
		
		if (_selectedT!=null) ClearSelection();
		
		_selectedProduct = GetSelectedProduct(t);
		
		Bounds bounds = GetBounds(t);
		if (t.childCount==0){
			_rotationCentre = t.position;
		}else{
			_rotationCentre = t.position + bounds.center;
		}
		
		if (UseSelectionBox(t)){
			ShowSelectBox(bounds, t);
		}else if (IsDriveway(t)){
			_driveway.Highlight(true);
		}else{
			//storedMaterials = SaveSelectedMaterials(t);
			Material mat = new Material(Shader.Find("Self-Illumin/Diffuse"));
			mat.color = new Color(0.8f, 0.8f, 0.8f);
			ApplyMaterial(t, mat, true);
			//UpdateMaterials(t, new Color(0.1f, 0.1f, 0.1f), true);
		}
		_selectedT = t;
	}
	
	Material [] SaveSelectedMaterials(Transform t){
		Material [] materials = new Material[t.gameObject.GetComponent<Renderer>().materials.Length];
		for(int i=0; i<t.gameObject.GetComponent<Renderer>().materials.Length; i++){
			materials[i] = new Material(t.gameObject.GetComponent<Renderer>().materials[i]);
		}
		return materials;
	}
	
	public void ClearSelection(){
		if (_selectBox != null && _selectBox.activeSelf){
			_selectBox.SetActive (false);
			_selectBox.transform.parent = null;
		}else{
			if (IsDriveway(_selectedT)){
				_driveway.Highlight(false);
			}else{
				//				Debug.Log ("DrivewayController.ClearSelection '" + _selectedProduct.Type + "'");
				Material mat = GetMaterial();
				if (mat!=null) ApplyMaterial(_selectedT, mat, false);
				if (_selectedProduct != null && _selectedProduct.ProductName == "manhole_cover") {
					Renderer renderer = _selectedT.gameObject.GetComponent<Renderer> ();
					renderer.materials [1].shader = Shader.Find ("Legacy Shaders/Transparent/Diffuse");
					renderer.materials [1].color = new Color (1, 1, 1, 0);
				}
				//UpdateMaterials(_selectedT, new Color(0.1f, 0.1f, 0.1f), false);
				//RestoreMaterials();
			}
		}
		_selectedT = null;
		_selectedProduct = null;
	}
		
	public void UpdateMaterialsOnLoad(GameObject go){
		Material mat = new Material(Shader.Find("Diffuse"));
		ApplyMaterial (go.transform, mat, false);
	}

	Material GetMaterial(){
		Material mat = null;
		if (_selectedProduct == null){
			mat = new Material(Shader.Find("Diffuse"));
			Debug.Log ("DrivewayController.GetMaterial Diffuse");
		}else if(_selectedProduct.ProductName.Equals ("water")){
			Debug.Log ("DrivewayController.GetMaterial FX/Water");
			GameObject water = GameObject.Find ("Water");
			_selectedT.gameObject.GetComponent<Renderer>().material = water.GetComponent<Renderer>().material;
		}else if (_selectedProduct.Type.Equals ("boundary")||_selectedProduct.Type.Equals ("vegetation")){
			Debug.Log ("DrivewayController.GetMaterial Transparent/Diffuse Product.Type:" + _selectedProduct.Type);
			mat = new Material(Shader.Find("Transparent/Diffuse"));
		}else{
			Debug.Log ("DrivewayController.GetMaterial Diffuse Product.Type:" + _selectedProduct.Type);
			mat = new Material(Shader.Find("Diffuse"));
		}
		return mat;
	} 
	
	void UpdateMaterials(Transform t, Color col, bool add){
		//Texture tex;
		
		if (t!=null && t.gameObject!=null && t.gameObject.GetComponent<Renderer>()!=null){
			if (t.gameObject.GetComponent<Renderer>().materials.Length>1){
				Material [] materials = new Material[t.gameObject.GetComponent<Renderer>().materials.Length];
				for(int i=0; i<t.gameObject.GetComponent<Renderer>().materials.Length; i++){
					//tex = t.gameObject.renderer.materials[i].mainTexture;
					if (add){
						materials[i].color += col;
					}else{
						materials[i].color -= col;
					}
				}
				
			}
		}else{
			Debug.Log ("DrivewayController.ApplyMaterial something null " + t);
		}
		
		if (t!=null && t.childCount>0){
			for(int i=0; i<t.childCount; i++){
				Transform tt = t.GetChild (i);
				if (tt!=null) UpdateMaterials(tt, col, add);
			}
		}
	}
	
	public void ApplyMaterial(Transform t, Material mat, bool selected){
		Texture tex;
		
		if (t!=null && t.gameObject!=null && t.gameObject.GetComponent<Renderer>()!=null){
			if (t.gameObject.GetComponent<Renderer>().materials.Length>1){
				Material [] materials = new Material[t.gameObject.GetComponent<Renderer>().materials.Length];
				for(int i=0; i<t.gameObject.GetComponent<Renderer>().materials.Length; i++){
					tex = t.gameObject.GetComponent<Renderer>().materials[i].mainTexture;
					//materials[i] = mat;
					if (selected){
						materials[i] = new Material(Shader.Find("Self-Illumin/Diffuse"));
						materials[i].color = new Color(0.8f, 0.8f, 0.8f);
					}else{
						materials[i] = new Material(Shader.Find("Diffuse"));
						if (_selectedProduct!=null && (_selectedProduct.Type.Equals("circle"))){
							materials[i].color = new Color(0.8f, 0.8f, 0.8f);
						}else{
							//materials[i].color = new Color(1.0f, 1.0f, 1.0f);
							materials[i].color = new Color(0.8f, 0.8f, 0.8f);
						}
					}
					materials[i].mainTexture = tex;
				}
				t.gameObject.GetComponent<Renderer>().materials = materials;
			}else{
				Material material;
				tex = t.gameObject.GetComponent<Renderer>().material.mainTexture;
				Vector2 scale = t.gameObject.GetComponent<Renderer>().material.mainTextureScale;
				if (selected){
					material = new Material(Shader.Find("Self-Illumin/Diffuse"));
					material.color = new Color(0.8f, 0.8f, 0.8f);
				}else{
					material = new Material(Shader.Find("Diffuse"));
					if (_selectedProduct!=null && 
					    (_selectedProduct.Type.Equals("circle") || _selectedProduct.Type.Equals ("octant") || 
					 (_selectedProduct.Type.Equals ("paving") && _selectedProduct.Pattern.StartsWith("flag_")))){
						material.color = new Color(0.8f, 0.8f, 0.8f);
					}
				}
				material.mainTexture = tex;
				material.mainTextureScale = scale;
				t.gameObject.GetComponent<Renderer>().material = material;
			}
		}else{
			Debug.Log ("DrivewayController.ApplyMaterial something null " + t);
		}
		
		if (t!=null && t.childCount>0){
			for(int i=0; i<t.childCount; i++){
				Transform tt = t.GetChild (i);
				if (tt!=null) ApplyMaterial(tt, mat, selected);
			}
		}
	}

	bool IsDriveway(Transform t){
		if (t==null || _driveway==null || _driveway.DrivewayGO==null) return false;
		return (_driveway.DrivewayGO.transform == t);
	}

	bool IsEdging(Transform t){
		if (t==null || _edging==null) return false;
		bool result = false;
		foreach(Edging edging in _edging){
			if (edging.EdgingGO && edging.EdgingGO.transform==t){
				result = true;
				break;
			}
		}
		return result;
	}

	public void ClearSelectBox(){
		_selectBox.transform.parent = null;
		_selectBox.SetActive (false);
		_selectedT = null;
	}
	
	public void ShowSelectBox(Bounds bounds, Transform t){
		
		//Debug.Log ("DrivewayController.ShowSelectBox bounds:" + bounds);
		
		_selectBox.SetActive (true);
		_selectBox.transform.position = t.position;
		_selectBox.transform.rotation = t.rotation;
		_selectBox.transform.parent = t;
		Vector3 pos = bounds.center;
		pos.y = bounds.extents.y;
		_selectBox.transform.localPosition = pos;
		
		Vector3 size = bounds.size;
		size *= 1.001f;
		size.y += 0.1f;
		Vector3 extents = size/2.0f;
		
		_selectBox.transform.GetChild (0).transform.localPosition = new Vector3(extents.x, -extents.y, extents.z);
		_selectBox.transform.GetChild (1).transform.localPosition = new Vector3(-extents.x, -extents.y, extents.z);
		_selectBox.transform.GetChild (2).transform.localPosition = new Vector3(-extents.x, extents.y, extents.z);
		_selectBox.transform.GetChild (3).transform.localPosition = new Vector3(extents.x, extents.y, extents.z);
		_selectBox.transform.GetChild (4).transform.localPosition = new Vector3(extents.x, -extents.y, -extents.z);
		_selectBox.transform.GetChild (5).transform.localPosition = new Vector3(-extents.x, -extents.y, -extents.z);
		_selectBox.transform.GetChild (6).transform.localPosition = new Vector3(extents.x, extents.y, -extents.z);
		_selectBox.transform.GetChild (7).transform.localPosition = new Vector3(-extents.x, extents.y, -extents.z);
		_selectBox.transform.GetChild (8).transform.localPosition = Vector3.zero;
		_selectBox.transform.GetChild (8).transform.localScale = size;
		
		_selectedT = t;
	}

	// Update is called once per frame
	void Update () {
		if (DV_UI.ModalActive) return;

		if (_active && _modified){
			float elapsedTime = Time.time - _modifiedTime;
			if (elapsedTime > 5){
				StartCoroutine(GrabThumbnail(true));
				//DVWebService.SaveProject (_project);
				//Modified = false;
			}
		}

		if (_driveway != null && _driveway.Loading) _driveway.CheckTextureLoading();

		Vector3 pos;
		Vector3 curScreenPoint;
		float theta;
		bool doubleClick = false;
		if (Input.GetMouseButtonDown(0) && !_mouseDown){
			_mouseDown = true;
			float elapsedTime = Time.time - _clickTime;
			doubleClick = (elapsedTime<0.3f);
			_clickTime = Time.time;
			if (_selectedT!=null){
				_screenPoint = Camera.main.WorldToScreenPoint(_selectedT.position);
				pos = Camera.main.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, _screenPoint.z));
				_offset1 = _selectedT.position - pos;
				_initDragMousePos = Input.mousePosition;
				if (_selectedProduct!=null) _offset2 = _selectedProduct.Position2d - Input.mousePosition;
			}
			if (_mouseMode == (int)MouseModes._NONE) _camMousePos = new Vector2(Input.mousePosition.x, Input.mousePosition.y);
		}else if (Input.GetMouseButtonUp (0)){
			_mouseDown = false;
		}

		if (doubleClick) Debug.Log ("DrivewayController.Update doubleClick");
		
		if (Input.GetMouseButtonDown(0) && _mouseMode==(int)MouseModes._ADD_FEATURE){
			pos = MouseToDriveway (Input.mousePosition);
			Debug.Log ("DrivewayController.Update ADD_FEATURE pos:" + pos);
			pos.y += 0.1f;
			_storedProduct.Position = pos;
			_storedProduct.Position2d = Input.mousePosition;
			_storedProduct.Name = string.Format("product_{0:000}", _project.NextID());
			_mouseMode = (int)MouseModes._NONE;
			_project.Add(_storedProduct);
			Feature feature = new Feature (this, _storedProduct);
			if (_features != null)
				_features.Add (feature);
			Modified = true;
			DV_UI.DesignController.transform.Find ("Footer/Help3").gameObject.SetActive (false);
		}else if (Input.GetMouseButtonDown(0) && _mouseMode==(int)MouseModes._ADD_ACCESSORY){
			pos = MouseToDriveway (Input.mousePosition);
			Debug.Log ("DrivewayController.Update ADD_ACCESSORY pos:" + pos);
			pos.y += 0.1f;
			_storedProduct.Position = pos;
			_storedProduct.Position2d = Input.mousePosition;
			_storedProduct.Name = string.Format("product_{0:000}", _project.NextID());
			_mouseMode = (int)MouseModes._NONE;
			string name = (_storedProduct.Color == "") ? (_storedProduct.Type + "_" + _storedProduct.ProductName) : (_storedProduct.Type + "_" + _storedProduct.ProductName + "_" + _storedProduct.Color);
			WWWAsset asset = new WWWAsset (_storedProduct, name);
			if (_assets != null)
				_assets.Add (asset);
			_project.Add(_storedProduct);
			Modified = true;
			DV_UI.DesignController.transform.Find ("Footer/Help3").gameObject.SetActive (false);
		}else if (!DV_UI.ButtonCapturedMouse && _drawShape==null && Input.mousePosition.y>_mouseLegal[0] && Input.mousePosition.y<_mouseLegal[1]){
			if (_mouseMode==(int)MouseModes._TILT_PLANE || _mouseMode==(int)MouseModes._MOVE_PLANE || _mouseMode==(int)MouseModes._ROTATE_PLANE ){ 
				//Handled in the section below
			}else if (_mouseMode==(int)MouseModes._MOVE_PHOTO || _mouseMode==(int)MouseModes._SCALE_PHOTO || _mouseMode==(int)MouseModes._ROTATE_PHOTO){
				//Handled in the section below
			}else if(_mouseMode==(int)MouseModes._DRAW_OUTLINE){
				//Handled by DrawShape
			}else if (Input.GetMouseButtonDown(0)){
				//Debug.Log("Mouse is down " + Input.mousePosition);
				
				RaycastHit hitInfo = new RaycastHit();
				bool hit = Physics.Raycast(Camera.main.ScreenPointToRay(Input.mousePosition), out hitInfo, 1000000, 1);
				
				if (hit) 
				{
					Transform t = hitInfo.transform;
					while(t.parent && t.parent!=gameObject.transform) t = t.parent;
					
					if (doubleClick){
						Debug.Log("Hit " + hitInfo.transform.gameObject.name + " " + t.gameObject.name + " " + (_dv_ui!=null));
						//Bounds bounds = GetRBounds(t);
						//ShowSelectBox(bounds, t);
						if (t != _selectedT){
							ShowSelection(t);
							if (_dv_ui!=null){
								if (_selectedProduct!=null){
									_dv_ui.ObjectSelected(true, _selectedProduct.Type);
								}
							}
							_mouseMode = (int)MouseModes._MOVE;
						}
						_screenPoint = Camera.main.WorldToScreenPoint(_selectedT.position);
						pos = Camera.main.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, _screenPoint.z));
						_offset1 = _selectedT.position - pos;
						_initDragMousePos = Input.mousePosition;
					}else if (_selectedProduct != GetSelectedProduct(t)){
						Debug.Log ("DrivewayController.Update ClearSelection");
						Product product = _selectedProduct;
						ClearSelection();
						_selectedProduct = product;
						if (_dv_ui!=null) _dv_ui.ObjectSelected(false);
						_mouseMode = (int)MouseModes._NONE;
					}
				} else {
					//ClearSelectBox();
					Debug.Log ("DrivewayController.Update ClearSelection");
					ClearSelection();
					if (_dv_ui!=null) _dv_ui.ObjectSelected(false);
					_mouseMode = (int)MouseModes._NONE;
				}
			} 

			Vector3 delta;
			Transform imageT;

			if (Input.GetMouseButton (0) && Input.mousePosition.y>_mouseLegal[0] && Input.mousePosition.y<_mouseLegal[1]){
				switch(_mouseMode){
				case (int)MouseModes._NONE:
					//Debug.Log ("DrivewayController.Update MouseModes._NONE _cameraScript.UseMouseMove:" + _cameraScript.UseMouseMove);
					//if (_cameraScript!=null && _cameraScript.Interactive){
					//Vector3 camOffset = (new Vector2(Input.mousePosition.x, Input.mousePosition.y) - _camMousePos) * Time.deltaTime;
					//	_cameraScript.PointCamera(camOffset);
					//}
					break;
				case (int)MouseModes._MOVE_PLANE:
					delta = Input.mousePosition - _prevMousePosition;
					delta.z = 0;
					_plane.transform.Translate(delta * Time.deltaTime);
					Modified = true;
					break;
				case (int)MouseModes._TILT_PLANE:
					delta = Input.mousePosition - _prevMousePosition;
					delta.x = delta.y;
					delta.y = delta.z = 0;
					_plane.transform.Rotate(delta * Time.deltaTime);
					Modified = true;
					break;
				case (int)MouseModes._ROTATE_PLANE:
					delta = Input.mousePosition - _prevMousePosition;
					delta.z = delta.y;
					delta.y = delta.x;
					delta.x = 0;
					_plane.transform.Rotate(delta * Time.deltaTime);
					Modified = true;
					break;
				case (int)MouseModes._MOVE_PHOTO:
					delta = Input.mousePosition - _prevMousePosition;
					delta.z = 0;
					imageT = GetImageTransform();
					imageT.Translate(delta);
					SyncThumbnailImage(imageT);
					Modified = true;
					break;
				case (int)MouseModes._SCALE_PHOTO:
					delta = Input.mousePosition - _prevMousePosition;
					delta.x *= 0.05f;
					delta.y = delta.x;
					delta.z = 0;
					imageT = GetImageTransform();
					imageT.localScale += delta;
					SyncThumbnailImage(imageT);
					Modified = true;
					break;
				case (int)MouseModes._ROTATE_PHOTO:
					delta = Input.mousePosition - _prevMousePosition;
					delta.z = -delta.x * 0.5f;
					delta.x = delta.y = 0;
					imageT = GetImageTransform();
					imageT.Rotate(delta);
					SyncThumbnailImage(imageT);
					Modified = true;
					break;
				case (int)MouseModes._MOVE:
					if (_selectedT && !IsDriveway(_selectedT)){
						float diff = Input.mousePosition.y - _initDragMousePos.y;
						curScreenPoint = new Vector3(Input.mousePosition.x, Input.mousePosition.y, _screenPoint.z + diff/30);
						pos = Camera.main.ScreenToWorldPoint(curScreenPoint) + _offset1;
						pos = MapToPlane(pos);
						if (IsEdging(_selectedT)){
						}else{
							if (_selectedProduct!=null){
								_selectedProduct.Position = pos * 1000;
								_selectedProduct.Position2d = Input.mousePosition + _offset2;
							}else{
								Debug.Log ("DrivewayController.Update no _selectedProduct");
							}
							_selectedT.position = pos;
						}
						Modified = true;
					}
					break;
				case (int)MouseModes._ROTATE://rotate
					theta = Input.mousePosition.x - _prevMousePosition.x;
					if (_selectedT){
						//_selectedT.RotateAround(_selectedT.position, _selectedT.up, theta * Time.deltaTime * 10);
						_selectedT.Rotate(Vector3.up * theta * Time.deltaTime * 10);
						if (_selectedProduct!=null){
							_selectedProduct.Position = _selectedT.position * 1000;
							_selectedProduct.Rotation = _selectedT.rotation.eulerAngles;
						}
						Modified = true;
					}
					break;
				case (int)MouseModes._MOVE_PATTERN://pattern move
					if (_selectedT){
						if (IsDriveway(_selectedT)){
							delta = Input.mousePosition - _prevMousePosition;
							Vector2 d = new Vector2(delta.x * Time.deltaTime, delta.y * Time.deltaTime);
							_driveway.MoveTexture( d.x, d.y);
							Modified = true;
						}
					}
					break;
				case (int)MouseModes._ROTATE_PATTERN://pattern rotate
					if (_selectedT){
						if (IsDriveway(_selectedT)){
							delta = Input.mousePosition - _prevMousePosition;
							_driveway.RotateTexture( delta.x * Time.deltaTime);
							Modified = true;
						}
					}
					break;
				case (int)MouseModes._FLIP://flip
					if (_selectedT){
						Debug.Log ("DrivewayController.Update flip");
					}
					break;
				}
			}
		}

		_prevMousePosition = Input.mousePosition;
	}

	public void SaveOnBack(){
		StartCoroutine(GrabThumbnail(true, true));
	}

	Transform GetImageTransform(){
		return GameObject.Find("CanvasBehind3D/Image").transform;
	}

	Vector3 MapToPlane(Vector3 pt){
		//Take a point in World space and find where a ray intersects the driveway_plane
		Vector3 org = new Vector3(pt.x, 100000, pt.z);
		Vector3 down = new Vector3(0, -1, 0);
		RaycastHit info;
		int layerMask = 1 << 8;
		Vector3 result = new Vector3(pt.x, pt.y, pt.z);
		if (Physics.Raycast ( org, down, out info, 1000000, layerMask)){
			result = info.point;
			result.y += 0.1f;
		}
		return result;
	}

	public void AddProduct(string type){
		if (type.Equals ("feature")){
			XmlDocument doc = new XmlDocument();
			doc.LoadXml("<product type=\"octant\" product=\"saxon_2_ring\" color=\"b\" pattern=\"\" size=\"\" aspectratio=\"\" " +
			            "coping=\"\" copingcolor=\"\" displayname=\"Saxon 2-ring Octant\" displaycolor=\"Buff (B)\" " + 
			            "displaypattern=\"\" colorswatch=\"\" patternswatch=\"\" name=\"product_" + _project.Products.Count + 
			            "\" positionx=\"0\" positiony=\"0\" positionz=\"0\" rotation=\"0\" patternpositionx=\"0\" patternpositionz=\"0\" " +
			            "patternrotation=\"0\"></product>");
			Product product = new Product(doc.FirstChild);
			Feature feature = new Feature(this, product);
			if (_features!=null) _features.Add (feature);
		}else if (type.Equals ("accessory")){
			XmlDocument doc = new XmlDocument();
			doc.LoadXml("<product type=\"accessory\" product=\"step_haworth_moor_sawn\" color=\"abm\" pattern=\"\" size=\"1850\" " +
			            "aspectratio=\"\" coping=\"\" copingcolor=\"\" displayname=\"Fairstone Step\" " + 
			            "displaycolor=\"Antique Silver Multi (ASM)\" displaypattern=\"\" " +
			            "colorswatch=\"swatch/paving_haworth_moor_sawn_asm.jpg\" patternswatch=\"\" name=\"product_" + 
			            _project.Products.Count + "\" positionx=\"0\" positiony=\"0\" positionz=\"0\" rotation=\"0\" " +
			            "patternpositionx=\"0\" patternpositionz=\"0\" patternrotation=\"0\"></product>");
			Product product = new Product(doc.FirstChild);
			string name = (product.Color=="") ? (product.Type + "_" + product.ProductName) : (product.Type + "_" + product.ProductName + "_" + product.Color);
			WWWAsset asset = new WWWAsset(product, name );
			if (_assets!=null) _assets.Add (asset );
		}else if (type.Equals ("building")){
			XmlDocument doc = new XmlDocument();
			doc.LoadXml("<product type=\"summerhouse\" product=\"alton_stratford\" color=\"\" pattern=\"\" size=\"\" aspectratio=\"\" " +
			            "coping=\"\" copingcolor=\"\" displayname=\"Summerhouse\" displaycolor=\"\" displaypattern=\"\" " +
			            "colorswatch=\"\" patternswatch=\"\" name=\"product_" + _project.Products.Count + "\" positionx=\"0\" positiony=\"0\" positionz=\"0\" " +
			            "rotation=\"0\" patternpositionx=\"0\" patternpositionz=\"0\" patternrotation=\"0\"></product>");
			Product product = new Product(doc.FirstChild);
			string name = (product.Color=="") ? (product.Type + "_" + product.ProductName) : (product.Type + "_" + product.ProductName + "_" + product.Color);
			WWWAsset asset = new WWWAsset(product, name );
			if (_assets!=null) _assets.Add (asset );
		}else if (type.Equals ("tree")){
			XmlDocument doc = new XmlDocument();
			int idx = Random.Range(2, 4);
			doc.LoadXml("<product type=\"vegetation\" product=\"plant_" + idx + "\" color=\"\" pattern=\"\" size=\"\" aspectratio=\"\" " +
			            "coping=\"\" copingcolor=\"\" displayname=\"Plant " + idx + "\" displaycolor=\"\" displaypattern=\"\" " +
			            "colorswatch=\"thumb/vegetation_plant" + idx + ".jpg\" patternswatch=\"\" name=\"product_" + _project.Products.Count + 
			            "\" positionx=\"0\" positiony=\"0\" positionz=\"0\" rotation=\"0\" patternpositionx=\"0\" patternpositionz=\"0\" " +
			            "patternrotation=\"0\"></product>");
			Product product = new Product(doc.FirstChild);
			string name = (product.Color=="") ? (product.Type + "_" + product.ProductName) : (product.Type + "_" + product.ProductName + "_" + product.Color);
			WWWAsset asset = new WWWAsset(product, name );
			if (_assets!=null) _assets.Add (asset );
		}
	}
	//Nam.Nguyen Adding new function to revices data from xml 
	public void AddProduct(string type, string dataXML){
		dataXML = dataXML.Replace (" & ", " &#038; ");
		Debug.Log("AddProduct type:"+ type);
		Product product = null;
		
		if (type.Equals ("feature")){
			XmlDocument doc = new XmlDocument();
			doc.LoadXml(dataXML + " name=\"product_" + _project.Products.Count + "\" positionx=\"0\" positiony=\"0\" positionz=\"0\" " +
			            "rotation=\"0\" patternpositionx=\"0\" patternpositionz=\"0\" patternrotation=\"0\"></product>");
			Debug.Log(dataXML + " name=\"product_" + _project.Products.Count + "\" positionx=\"0\" positiony=\"0\" positionz=\"0\" " +
			          "rotation=\"0\" patternpositionx=\"0\" patternpositionz=\"0\" patternrotation=\"0\"></product>");
			product = new Product(doc.FirstChild);
			Feature feature = new Feature(this, product);
			if (_features!=null) _features.Add (feature);
		}else if (type.Equals ("accessory")){
			XmlDocument doc = new XmlDocument();
				doc.LoadXml(dataXML + " name=\"product_" + _project.Products.Count + "\" positionx=\"0\" positiony=\"0\" positionz=\"0\" " +
			            "rotation=\"0\" patternpositionx=\"0\" patternpositionz=\"0\" patternrotation=\"0\"></product>");
			Debug.Log(dataXML + " name=\"product_" + _project.Products.Count + "\" positionx=\"0\" positiony=\"0\" positionz=\"0\" " +
			          "rotation=\"0\" patternpositionx=\"0\" patternpositionz=\"0\" patternrotation=\"0\"></product>");
			product = new Product(doc.FirstChild);
			string name = (product.Color=="") ? (product.Type + "_" + product.ProductName) : (product.Type + "_" + product.ProductName + "_" + product.Color);
			WWWAsset asset = new WWWAsset(product, name );
			if (_assets!=null) _assets.Add (asset );
		}else if (type.Equals ("building")){
			XmlDocument doc = new XmlDocument();
				doc.LoadXml(dataXML + " name=\"product_" + _project.Products.Count + "\" positionx=\"0\" positiony=\"0\" positionz=\"0\" " +
			            "rotation=\"0\" patternpositionx=\"0\" patternpositionz=\"0\" patternrotation=\"0\"></product>");
			Debug.Log(dataXML + " name=\"product_" + _project.Products.Count + "\" positionx=\"0\" positiony=\"0\" positionz=\"0\" " +
			          "rotation=\"0\" patternpositionx=\"0\" patternpositionz=\"0\" patternrotation=\"0\"></product>");
			product = new Product(doc.FirstChild);
			string name = (product.Color=="") ? (product.Type + "_" + product.ProductName) : (product.Type + "_" + product.ProductName + "_" + product.Color);
			WWWAsset asset = new WWWAsset(product, name );
			if (_assets!=null) _assets.Add (asset );
		}else if (type.Equals ("tree")){
			XmlDocument doc = new XmlDocument();
			doc.LoadXml(dataXML + " name=\"product_" + _project.Products.Count + "\" positionx=\"0\" positiony=\"0\" positionz=\"0\" " +
			            "rotation=\"0\" patternpositionx=\"0\" patternpositionz=\"0\" patternrotation=\"0\"></product>");
			Debug.Log(dataXML + " name=\"product_" + _project.Products.Count + "\" positionx=\"0\" positiony=\"0\" positionz=\"0\" " +
			          "rotation=\"0\" patternpositionx=\"0\" patternpositionz=\"0\" patternrotation=\"0\"></product>");
			
			product = new Product(doc.FirstChild);
			string name = (product.Color=="") ? (product.Type + "_" + product.ProductName) : (product.Type + "_" + product.ProductName + "_" + product.Color);
			WWWAsset asset = new WWWAsset(product, name );
			if (_assets!=null) _assets.Add (asset );
		}else if (type.Equals ("post")){
			string xml = dataXML +  " name=\"product_" + _project.Products.Count + "\"></product>";
			Debug.Log("DrivewayController.AddProduct " + xml);
			XmlDocument doc = new XmlDocument();
			doc.LoadXml(xml);
			
			product = new Product(doc.FirstChild);
			//Post post = new Post(product);
			//if (_posts!=null) _posts.Add (post );
		}
		
		if (product != null){
			_project.Add(product);
			
			Modified = true;
		}
	}

	public static GameObject CloneGameObject(GameObject go){
		GameObject go1 = (GameObject)Instantiate(go);
		go1.SetActive (true);
		
		Modified = true;
		
		return go1;
	}

	public static void ChangeDriveway(Driveway driveway, Product product){
	}

	public static House GetHouseFromProduct(Product product){
		House house = null;

		return house;
	}

	public static JSONNode GetProductJSON(string name){
		if (ProductsJSON!=null){
			Debug.Log ("DrivewayController.GetProductJSON ProductsJSON count:" + ProductsJSON.Count.ToString () + " looking for " + name);
			for(int i=0; i<ProductsJSON.Count; i++){
				if (((string)ProductsJSON[i]["model"]).Equals(name)) return ProductsJSON[i];
			}
		}else{
			Debug.Log ("DrivewayController.GetProductJSON ProductsJSON null");
		}
		Debug.Log ("DrivewayController.GetProductJSON name not found");
		return null;
	}

	public void Clear(bool clearPlane=true){
		if (clearPlane){
			GameObject plane = GameObject.Find ("3D/driveway_plane");
			if (plane) Destroy(plane);
		}

		if (_dv_ui!=null){
			DV_UI.CanvasBehind.SetActive (true);
			Transform outlines = DV_UI.CanvasBehind.transform.Find ("Image");
			if (outlines!=null){
				while (outlines.childCount>0){
					GameObject child = outlines.GetChild (0).gameObject;
					child.transform.parent = null;
					Destroy(child);
				}
			}
			_dv_ui.ResetImage();
		}

		if (_driveway!=null){
			if (_driveway.PatternTexture!=null) Destroy(_driveway.PatternTexture);
			if (_driveway.DrivewayGO!=null){
				Destroy(_driveway.DrivewayGO);
				_driveway.DrivewayGO = null;
			}
		}
		_driveway = null;
		
		if (_features!=null){
			foreach(Feature feature in _features){
				if (feature.FeatureTexture!= null){
					Destroy(feature.FeatureTexture);
					feature.FeatureTexture = null;
				}
				if (feature.FeatureGO!=null){
					Destroy(feature.FeatureGO);
					feature.FeatureGO = null;
				}
			}
		}
		_features = new List<Feature>();
		
		if (_assets!=null){
			foreach(WWWAsset asset in _assets){
				if (asset.AssetGO!=null){
					Destroy(asset.AssetGO);
					asset.AssetGO = null;
				}
			}
		}
		_assets = new List<WWWAsset>();

		//Unload all assets in the bundle
		foreach(string key in _assetBundles.Keys){
			AssetBundle asset = _assetBundles [key];
			asset.Unload (true);
		}
		_assetBundles.Clear ();
		
		if (_edging!=null){
			foreach(Edging edging in _edging){
				if (edging.EdgingGOs!=null){
					Destroy(edging.EdgingGO);
					foreach(GameObject go in edging.EdgingGOs){
						Destroy(go);
						//go = null;
					}
				}
			}
		}
		_edging = new List<Edging>();

	}

	Vector3 RaycastPlane (Vector3 pos){
		Vector3 result = new Vector3 (pos.x, pos.y, pos.z);
		RaycastHit info;
		Vector3 down = new Vector3 (0, -1, 0);
		pos.y = 1000;
		int layerMask = 1 << 8;

		if (Physics.Raycast (pos, down, out info, 100000, layerMask)) {
			result = info.point * 1000;
			result.y += 0.1f;
		}

		return result;
	}

	public void Recreate(){
		Clear(false);

		Modified = true;
		_loading = true;

		foreach(Product product in _project.Products){
			Debug.Log ("DrivewayController.Init product.Type=" + product.Type);
			switch(product.Type){
			case "driveway":
			case "area":
				if (product.ProductName!=""){
					//if (_dv_ui!=null) _dv_ui.DrawOutline(product.SourceVertices2d);
					product.Name = "product_001";
					_driveway = new Driveway(_project, product);
				}else{
					_driveway = new Driveway(_project, product, false);
				}
				break;
			case "circle":
			case "octant":
				Feature feature = new Feature (this, product);
				product.Position = RaycastPlane (product.Position * 0.001f);
				if (_features!=null) _features.Add (feature);
				break;
			case "kerb":
			case "edging":
			case "edging_supported":
				if (product.SourceVertices2d != null && product.SourceVertices2d.Count > 1) {
					Edging edging = new Edging (product);
					if (_edging != null)
						_edging.Add (edging);
				}
				break;
			case "accessory":
			case "rock":
			case "summerhouse":
			case "greenhouse":
			case "vegetation":
				string assetname = (product.Color=="") ? (product.Type + "_" + product.ProductName) : (product.Type + "_" + product.ProductName + "_" + product.Color);
				product.Position = RaycastPlane (product.Position * 0.001f);
				WWWAsset asset = new WWWAsset(product, assetname );
				if (_assets!=null) _assets.Add (asset );
				break;
			}
		}
	}

	public void Init(Project project, bool newProject=false){
		Clear();

		_project = project;
		_active = true;
		Modified = newProject;
		_loading = true;
		GameObject go = GameObject.Find ("Canvas/Image");
		if (go!=null) go.SetActive(false);
		SetDefaultCamera();
		CreateDrivewayPlane();
		if (project.HouseDefault){
			DV_UI.SetDefaultHouse();
		}else{
			DVWebService.LoadProjectImage(project.HousePattern);
		}

		List<Product> removedProducts = new List<Product> ();

		foreach(Product product in project.Products){
			Debug.Log ("DrivewayController.Init product.Type=" + product.Type);
			switch(product.Type){
			case "driveway":
			case "area":
				if (product.ProductName!=""){
					//if (_dv_ui!=null) _dv_ui.DrawOutline(product.SourceVertices2d);
					product.Name = "product_001";
					_driveway = new Driveway(project, product);
				}else{
					_driveway = new Driveway(project, product, false);
				}
				break;
			case "circle":
			case "octant":
				if (product.Name != "" && product.Name.Length >= 11) {
					Feature feature = new Feature (this, product);
					if (_features != null)
						_features.Add (feature);
				} else {
					removedProducts.Add (product);
				}
				break;
			case "kerb":
			case "edging":
			case "edging_supported":
				if (product.SourceVertices2d != null && product.SourceVertices2d.Count > 1 && product.Name!="") {
					Edging edging = new Edging (product);
					if (_edging != null)
						_edging.Add (edging);
				}else {
					removedProducts.Add (product);
				}
				break;
			case "accessory":
			case "rock":
			case "summerhouse":
			case "greenhouse":
			case "vegetation":
				if (product.Name!=""){
					string assetname = (product.Color=="") ? (product.Type + "_" + product.ProductName) : (product.Type + "_" + product.ProductName + "_" + product.Color);
					WWWAsset asset = new WWWAsset(product, assetname );
					if (_assets!=null) _assets.Add (asset );
				}else {
					removedProducts.Add (product);
				}
				break;
			}
		}

		while(removedProducts.Count>0){
			Product product = removedProducts [0];
			removedProducts.RemoveAt (0);
			project.Products.Remove (product);
		}

		if (project.HousePosition.magnitude > 0.001f){
			Transform t = GetImageTransform();
			t.position = new Vector3(project.HousePosition.x, project.HousePosition.y, t.position.z);
			t.rotation = Quaternion.Euler(new Vector3(0,0,project.HouseRotation));
			t.localScale = new Vector3(project.HouseScale, project.HouseScale, project.HouseScale);
			SyncThumbnailImage(t);
		}
	}

	void SyncThumbnailImage(Transform t){
		//Positions the thumbnail image to match the CanvasBehind3D/Image transform
		try{
			Transform t1 = GameObject.Find("CanvasThumbnail/Image").transform;
			t1.position = t.position;
			t1.rotation = t.rotation;
			t1.localScale = t.localScale * 2.2f;
		}catch(NullReferenceException e){
			Debug.Log("DrivewayController.SyncThumbnailImage CanvasThumbnail/Image not found");
		}
	}

	void SetDefaultCamera(){
		_3d.transform.position = Vector3.zero;
		Camera.main.fieldOfView = 75.5f;
		Camera.main.transform.position = new Vector3(0, 0, -6.44f);
		Camera.main.transform.rotation = Quaternion.identity;
	}

	void CreateDrivewayPlane(){
		Vector3 [] vertices = new Vector3[4];
		Vector2 [] uv = new Vector2[4];

		GameObject go = new GameObject( "driveway_plane" );
		go.AddComponent<MeshFilter>();
		go.AddComponent<MeshRenderer>();
		
		MeshFilter filter = go.GetComponent<MeshFilter>();
		Mesh mesh = filter.mesh;

		vertices[0] = new Vector3(-100, 0, -450);
		vertices[1] = new Vector3(-100, 0,  500);
		vertices[2] = new Vector3( 100, 0,  500);
		vertices[3] = new Vector3( 100, 0, -450);
		uv[0] = new Vector2(0,1);
		uv[1] = new Vector2(0,0);
		uv[2] = new Vector2(1,0);
		uv[3] = new Vector2(1,1);
		int [] triangles = { 0, 1, 2, 0, 2, 3 };

		Texture tex = Resources.Load<Texture>("Textures/gridcell");
		mesh.vertices = vertices;
		mesh.uv = uv;
		mesh.triangles = triangles;

		/*Material material = new Material(Shader.Find ("Standard"));
		material.SetColor("_Color", new Color(1,1,1,0.5f));
		material.SetFloat("_Mode", 2);
		material.SetInt("_SrcBlend", (int)UnityEngine.Rendering.BlendMode.SrcAlpha);
		material.SetInt("_DstBlend", (int)UnityEngine.Rendering.BlendMode.OneMinusSrcAlpha);
		material.SetInt("_ZWrite", 0);
		material.SetInt("_ZTest", 1);
		material.DisableKeyword("_ALPHATEST_ON");
		material.EnableKeyword("_ALPHABLEND_ON");
		material.DisableKeyword("_ALPHAPREMULTIPLY_ON");
		material.renderQueue = 3000;*/
		Material material = new Material(Shader.Find("Custom/DrivewayPlane"));
		material.mainTexture = tex;
		material.mainTextureScale = new Vector2(100, 800);
		go.GetComponent<Renderer>().material = material;

		go.transform.SetParent ( transform, false );
		Vector3 pos = _project.PlanePosition * 0.001f;
		//pos.z = -pos.z;
		go.transform.position = pos;
		Vector3 rot = _project.PlaneRotation;
		//rot.x = -rot.x;
		//rot.y = - rot.y;
		Quaternion q = Quaternion.Euler (rot);
		if (_project.Version<3){
			q.z = -q.z;
			q.w = -q.w;
		}
		go.transform.rotation = q;

		go.AddComponent<MeshCollider>();
		go.layer = 8;
		go.GetComponent<Renderer>().enabled = false;

		_plane = go;
	}

	public void ShowPlane(bool show){
		if (show){
			gameObject.SetActive(true);
			for(int i=2; i<transform.childCount; i++) transform.GetChild (i).gameObject.SetActive (false);
			if (_plane){ 
				_plane.SetActive (true);
				Renderer renderer = _plane.GetComponent<Renderer>();
				renderer.enabled = true;
				_planePos = _plane.transform.position;
				_planeRot = _plane.transform.rotation;
			}
		}else{
			for(int i=2; i<transform.childCount; i++) transform.GetChild (i).gameObject.SetActive (true);
			if (_plane){
				//_plane.SetActive (false);
				Renderer renderer = _plane.GetComponent<Renderer>();
				renderer.enabled = false;
			}
		}
	}
	
	public void CreateNewProject(string name)
	{
		if (DV_UI.DesignActive && _project!=null){
			//Just resizing/renaming the project
			_project.Name = name;
			DV_UI.CloseModalPanel();
			return;
		}
			
		//_mouseMode = (int)MouseModes._DRAW_OUTLINE;
		//DV_UI.DrawShape.enabled = true;

		_project = new Project(name);
		if (_dv_ui!=null){
			DV_UI.CloseModalPanel();
			DV_UI.ShowModalPanel("Photo");
		}
	}

	public List<Vector3> Map2dToPlane(List<Vector3> vertices, bool debug=false){
		if (vertices==null || vertices.Count==0) return null;
		List<Vector3>mapped = new List<Vector3>();
		//for(int i=vertices.Count-1; i>=0; i--){
			//mapped.Add (pt * 0.001f);
		//	Vector3 pt = vertices[i];
		foreach(Vector3 pt in vertices){
			if (debug) AddDebugSphere(pt);
			mapped.Add (ScreenToDriveway(pt, false, debug));
		}
		return mapped;
	}

	public Vector3 MouseToDriveway(Vector3 pt, bool debug=false){
		Ray ray = Camera.main.ScreenPointToRay(pt);
		Vector3 mapped = Vector3.zero;
		int layerMask = 1 << 8;
		RaycastHit info;

		if (Physics.Raycast ( ray, out info, 100000, layerMask)){
			if (_up.magnitude==0) _up = info.normal;
			mapped = info.point;
			if (debug) AddDebugSphere(info.point, 0.15f, "blue");
		}
		return mapped * 1000;
	}

	Vector3 ScreenToDriveway(Vector3 pt, bool rescan=true, bool debug=false){
		Vector3 fwd = (pt - Camera.main.transform.position);
		Vector3 mapped = Vector3.zero;
		int layerMask = 1 << 8;
		RaycastHit info;

		if (Physics.Raycast ( Camera.main.transform.position, fwd, out info, 100000, layerMask)){
			if (_up.magnitude==0) _up = info.normal;
			mapped = info.point;
			if (debug) AddDebugSphere(info.point, 0.15f, "blue");
		}
		return mapped;
	}

	public void AddDebugSphere(Vector3 pt, float scale=20.0f, string col="red"){
		GameObject sphere = GameObject.CreatePrimitive(PrimitiveType.Sphere);
		sphere.transform.position = pt;
		sphere.transform.localScale = new Vector3(scale, scale, scale);
		Renderer renderer = sphere.GetComponent<Renderer>();
		GameObject go = GameObject.Find ("DebugSpheres");
		GameObject parent;
		if (go==null){
			go = new GameObject();
			go.name = "DebugSpheres";
			GameObject child = new GameObject();
			child.name = "red";
			child.transform.SetParent (go.transform);
			child = new GameObject();
			child.name = "blue";
			child.transform.SetParent (go.transform);
			child = new GameObject();
			child.name = "green";
			child.transform.SetParent (go.transform);
		}
		switch(col){
		case "blue":
			parent = GameObject.Find ("DebugSpheres/blue");
			sphere.transform.SetParent (parent.transform);
			renderer.material.color = Color.blue;
			break;
		case "green":
			parent = GameObject.Find ("DebugSpheres/green");
			sphere.transform.SetParent (parent.transform);
			renderer.material.color = Color.green;
			break;
		default:
			parent = GameObject.Find ("DebugSpheres/red");
			sphere.transform.SetParent (parent.transform);
			renderer.material.color = Color.red;
			break;
		}
	}

	Vector3 ScreenToWorld(Vector3 pt, bool rescan=true){
		Vector3 v = Vector3.zero;
		Vector3 fwd = pt - Camera.main.transform.position;
		int layerMask = 1 << 8;
		RaycastHit info;
		if (Physics.Raycast ( pt, fwd, out info, 100000, layerMask)){
			v = info.point;
			return v;
		}
		int count=0;
		if (rescan){
			Vector3 screenPos = Camera.main.WorldToScreenPoint(_project.PlanePosition * 0.001f);
			Vector3 delta = (screenPos - pt) * 0.05f; 
			while(v == Vector3.zero){
				pt += delta;
				if (Physics.Raycast ( pt, fwd, out info, 100000, layerMask)){
					v = info.point;
					break;
				}
				count++;
				if (count>20) break;
			}
		}
		return v;
	}

	Vector3 WorldToDriveway(Vector3 pt, bool rescan){
		GameObject go = new GameObject();
		Transform t = go.transform;
		t.position = pt - _project.PlanePosition * 0.001f;
		t.Rotate (0, 0, -_project.PlaneRotation.z);
		t.Rotate (-_project.PlaneRotation.x, 0, 0);
		pt = t.position + _project.PlanePosition;
		pt.y = 0;
		pt /= _project.PlaneSize;
		Destroy(go);
		return pt;
	}

	public GameObject CreateFeature(string type, Vector3 position, Quaternion rotation){
		Debug.Log ("DrivewayController CreateFeature " + type);
		GameObject go = null;
		
		if (type.Equals("square_feature")){
			if (_square_feature) go = (GameObject)Instantiate(_square_feature, position, rotation);
		}else if (type.Equals("circle_feature")){
			if (_circle_feature) go = (GameObject)Instantiate(_circle_feature, position, rotation);
		}else if (type.Equals("octant_feature")){
			if (_octant_feature) go = (GameObject)Instantiate(_octant_feature, position, rotation);
		}else if (type.Equals("bonding_feature")){
			if (_bonding_feature) go = (GameObject)Instantiate(_bonding_feature, position, rotation);
		}

		if (go!=null){
			go.AddComponent<MeshCollider>();
			go.transform.SetParent (transform, false);
			_modified = true;
		}

		return go;
	}

	public void DeleteSelected(bool removeFromLists=true){
		if (_selectedT == null) return;

		_selectBox.transform.parent = null;
		_selectBox.SetActive (false);

		bool found = false;
		Product product = null;
		string name = "";

		if (_selectedT == _driveway.DrivewayGO.transform) {
			Destroy (_driveway.DrivewayGO);
			return;
		}

		if (!found && _features!=null && _features.Count>0){
			foreach(Feature feature in _features){
				if (feature!=null && feature.FeatureGO == _selectedT.gameObject){
					found = true;
					name = feature.FeatureGO.name;
					Destroy(feature.FeatureGO);
					if (removeFromLists){
						_features.Remove (feature);
						product = feature.Product;
					}
					break;
				}
			}
		}
		if (!found && _assets!=null && _assets.Count>0){
			foreach(WWWAsset asset in _assets){
				if (asset!=null && asset.AssetGO == _selectedT.gameObject){
					found = true;
					name = asset.AssetGO.name;
					Destroy(asset.AssetGO);
					if (removeFromLists){
						_assets.Remove (asset);
						product = asset.Product;
					}
					break;
				}
			}
		}
		if (!found && _edging!=null && _edging.Count>0){
			foreach(Edging edging in _edging){
				if (edging!=null && edging.EdgingGO == _selectedT.gameObject){
					found = true;
					name = edging.EdgingGO.name;
					Destroy(edging.EdgingGO);
					if (removeFromLists){
						_edging.Remove (edging);
						product = edging.Product;
					}
					break;
				}
			}
		}

		if (product!=null) _project.Products.Remove (product);

		Modified = true;

		Debug.Log ("DrivewayController.DeleteSelected found:" + found + " name:" + name);
	}

	public void CreateEdging(){
		if (_storedProduct == null)
			return;

		RectTransform rt = (RectTransform)GameObject.Find("VectorCanvas").transform;
		Transform t = GameObject.Find("CanvasBehind3D/Image").transform;
		Vector2 imagesize = new Vector2(2062.0f, 1540.0f);
		float scale = imagesize.x/rt.sizeDelta.x;

		_storedProduct.SourceVertices2d = new List<Vector2> ();
		for (int i = 0; i < DV_UI.DrawShape.SourceVertices2d.Count; i++) {
			Vector2 v = DV_UI.DrawShape.SourceVertices2d [i];
			Vector3 pt = new Vector3(v.x, v.y);
			pt *= scale;
			pt.x -= imagesize.x/2;
			pt.y -= imagesize.y/2;
			pt.z = t.position.z;
			pt = t.InverseTransformPoint(pt);
			pt.z = 0;
			pt.y = 1590 - 1590/2 - pt.y;
			pt.x += 1590/2;
			_storedProduct.SourceVertices2d.Add(pt);
		}
		_storedProduct.Name = string.Format("product_{0:000}", _project.NextID());
		Edging edging = new Edging (_storedProduct);
		_edging.Add (edging);
		_project.Add (_storedProduct);
		Modified = true;

	}

	public static void NewProduct(XmlNode node){
		Debug.Log (string.Format ("DrivewayController.NewProduct {0}", node.InnerXml));
		Dictionary<string, string> details = GetDictionaryFromXml (node);

		Product product = new Product(details);
		Analytics.ga ("Project", "Add/modify product", string.Format("{0} {1}", product.Type, product.DisplayName));
		string name;
		WWWAsset asset;
		DrivewayController dc = GameObject.Find("3D").GetComponent<DrivewayController>();

		switch(product.Type){
		case "paving":
		case "driveway":
			//gardenScript.DrawShape ("paving", product);
			dc.MouseMode = (int)MouseModes._DRAW_OUTLINE;
			break;
		case "edging":
		case "edging_supported":
		case "kerb":
			if (DesignController != null) {
				_designController.SetFooterState ("draw_kerb");
				dc.MouseMode = (int)MouseModes._DRAW_OUTLINE;
				DV_UI.DrawShape.Close = false;
				DV_UI.DrawShape.enabled = true;
			}
			break;
		case "circle":
		case "octant":
			//Feature feature = new Feature (dc, product);
			//if (dc._features != null)
			//	dc._features.Add (feature);
			dc.MouseMode = (int)MouseModes._ADD_FEATURE;
			DV_UI.DesignController.transform.Find ("Footer/Help3").gameObject.SetActive (true);
			break;
		case "accessory":
			//name = (product.Color == "") ? (product.Type + "_" + product.ProductName) : (product.Type + "_" + product.ProductName + "_" + product.Color);
			//asset = new WWWAsset (product, name);
			//if (_assets != null)
			//	_assets.Add (asset);
			dc.MouseMode = (int)MouseModes._ADD_ACCESSORY;
			DV_UI.DesignController.transform.Find ("Footer/Help3").gameObject.SetActive (true);
			break;
		default:
			DV_UI.ShowAlert("Warning", string.Format("Type {0} not supported", product.Type));
			break;
		}

		if (product != null){
			//_project.Add(product);
			//Modified = true;
			dc._storedProduct = product;
		}
	}

	public static Dictionary<string, string> GetDictionaryFromXml(XmlNode node){
		Dictionary<string, string> details = new Dictionary<string, string>();
		StringBuilder str = new StringBuilder();

		if (node!=null && node.Attributes!=null){
			bool found = false;
			int count = 0;
			do{
				foreach(XmlAttribute attr in node.Attributes){
					try{
						if (details.ContainsKey(attr.Name)){
							details[attr.Name] = attr.Value;
						}else{
							details.Add (attr.Name, attr.Value);
						}
						str.Append(string.Format ("{0}:{1} ", attr.Name, attr.Value));
						if (node.ParentNode!=null) CheckParentAttributes(attr, node.ParentNode, details);
					}catch(ArgumentException e){
						Debug.Log (string.Format("DrivewayController.GetDictionaryFromXml {0}:{1}", attr.Name, attr.Value));
					}
				}
				node = node.ParentNode.ParentNode;
				while(node!=null && node.Name!="option") node = node.ParentNode;
				found = false;
				count++;
				if (node!=null && node.HasChildNodes){
					for(int i=0; i<node.ChildNodes.Count; i++){
						if (node.ChildNodes[i].Name=="productproperties"){
							found = true;
							node = node.ChildNodes[i];
							break;
						}
					}
				}
				if (found==false){
					while(node!=null && node.Name!="option") node = node.ParentNode;
				}
			}while(node!=null && count<20);
			Debug.Log ("DrivewayController.GetDictionaryFromXml " + str);
		}


		return details;
	}

	static void CheckParentAttributes(XmlAttribute attr, XmlNode parent, Dictionary<string,string>details){
		XmlNode parentAttr;
		switch(attr.Name){
		case "color":
			parentAttr = parent.Attributes.GetNamedItem("name");
			if (parentAttr!=null && !details.ContainsKey("displaycolor")){
				details.Add ("displaycolor", parentAttr.Value);
			}
			parentAttr = parent.Attributes.GetNamedItem("image");
			if (parentAttr!=null && !details.ContainsKey("colorswatch")){
				details.Add ("colorswatch", parentAttr.Value);
			}
			break;
		case "pattern":
			parentAttr = parent.Attributes.GetNamedItem("name");
			if (parentAttr!=null && !details.ContainsKey("displaypattern")){
				details.Add ("displaypattern", parentAttr.Value);
			}
			parentAttr = parent.Attributes.GetNamedItem("image");
			if (parentAttr!=null && !details.ContainsKey("patternswatch")){
				details.Add ("patternswatch", parentAttr.Value);
			}
			break;
		}
	}

	public static void ChangeProduct(XmlNode node){

		Dictionary<string, string> details = GetDictionaryFromXml(node);

		Debug.Log (string.Format ("DrivewayController.ChangeProduct {0}", details.ToString()));
		GameObject go = GameObject.Find("3D");
		DrivewayController controller = go.GetComponent<DrivewayController>();

		if (controller.SelectedProduct!=null){
			Product product = controller.SelectedProduct;
			Product newProduct = new Product (details);
			switch(controller.SelectedProduct.Type){
			case "driveway":
			case "paving":
				product.Pattern = details["pattern"];
				product.Color = (details.ContainsKey("color")) ? details["color"] : "";
				product.RandomCount = (details.ContainsKey("random")) ? Int32.Parse(details["random"]) : 1;
				product.ModifyButtons = (details.ContainsKey("modifybuttons")) ? details["modifybuttons"] : "";
				product.ProductName = details["product"];
				product.DisplayName = (details.ContainsKey("displayname")) ? details["displayname"] : "";
				product.DisplayColor = (details.ContainsKey("displaycolor")) ? details["displaycolor"] : "";
				product.DisplayPattern = (details.ContainsKey("displaypattern")) ? details["displaypattern"] : "";
				product.ColorSwatch = (details.ContainsKey("colorswatch")) ? details["colorswatch"] : "";
				product.PatternSwatch = (details.ContainsKey("patternswatch")) ? details["patternswatch"] : "";
				if (controller.SelectedT==null && controller.Driveway!=null && controller.Driveway.DrivewayGO!=null){
					controller.SelectedT = controller.Driveway.DrivewayGO.transform;
				}
				controller.DeleteSelected();
				Driveway driveway = new Driveway(_project, product);
				Modified = true;
				driveway.AutoSelect = true;
				_project.Add(product);
				controller.Driveway = driveway;
				break;
			case "edging":
			case "edging_supported":
			case "kerb":
				Edging edging = FindEdging (controller);
				edging.Change (newProduct);
				edging.AutoSelect = true;
				break;
			case "circle":
			case "octant":
				Feature feature = FindFeature (controller);
				feature.Change (newProduct);
				feature.AutoSelect = true;
				break;
			case "accessory":
				string name = (newProduct.Color == "") ? (newProduct.Type + "_" + newProduct.ProductName) : (newProduct.Type + "_" + newProduct.ProductName + "_" + newProduct.Color);
				WWWAsset asset = new WWWAsset (product, name);
				if (_assets != null)
					_assets.Add (asset);
				_project.Add (newProduct);
				controller.DeleteSelected ();
				asset.AutoSelect = true;
				//GV_UI.DesignController.ShowSelectedBtn (FooterButtons.ACCESSORIES);
				break;
			}
			Modified = true;
		}
	}

	static Feature FindFeature(DrivewayController dc){
		if (dc._features == null || dc._features.Count == 0 || dc.SelectedT == null)
			return null;
		foreach(Feature feature in dc._features){
			if (feature!=null && feature.FeatureGO == dc.SelectedT.gameObject){
				return feature;
			}
		}
		return null;
	}

	static Edging FindEdging(DrivewayController dc){
		if (dc._edging == null || dc._edging.Count == 0 || dc.SelectedT == null)
			return null;
		foreach(Edging edging in dc._edging){
			if (edging!=null && edging.EdgingGO == dc.SelectedT.gameObject){
				return edging;
			}
		}
		return null;
	}

	public void RestorePlaneTransform(){
		_plane.transform.position = _planePos;
		_plane.transform.rotation = _planeRot;
	}

	public void HouseImageCallback(string str) {
		Debug.Log ("DrivewayController.HouseImageCallback " + str);
		JSONNode json = JSON.Parse(str);
		GameObject go = GameObject.Find("Canvas/ModalPanels/PhotoPanel");
		PhotoController controller = go.GetComponent<PhotoController>();
		if (controller!=null) controller.WebImageCallback(json["url"], json["guid"]);
	}

	public IEnumerator GrabThumbnail(bool saveProject=true, bool back=false)
	{
		Camera cam = GameObject.Find("ThumbnailCamera").GetComponent<Camera>();
		cam.Render();

		yield return new WaitForEndOfFrame();

		if (saveProject){
			RenderTexture rtex = (RenderTexture)cam.targetTexture;
			RenderTexture prev = RenderTexture.active;
			RenderTexture.active = rtex;
			Texture2D tex = new Texture2D(rtex.width, rtex.height);
			tex.ReadPixels(new Rect(0,0,tex.width,tex.height), 0, 0);
			string thumbnail = System.Convert.ToBase64String(tex.EncodeToPNG());
			_project.Thumbnail = thumbnail;
			DVWebService.SaveProject(_project, back);
			Modified = false;
		}

		if (back) {
			DV_UI.DrawShape.Clear();
			DV_UI.Animate("designOut");
		}
	}
}
